#from django.conf.urls import patterns, include, url
#from django.contrib import admin
#
#urlpatterns = patterns('',
#    # Examples:
#    # url(r'^$', 'sinehr.views.home', name='home'),
#    # url(r'^blog/', include('blog.urls')),
#
#    url(r'^admin/', include(admin.site.urls)),
#)
#

from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_views
from django.contrib import admin
from rest_framework.routers import DefaultRouter

# Create a router and register our viewsets with it.
from crux import views as crux_views
from audit import views as audit_views
from entities import views as entities_views
from encounters import views as encounters_views
from system import views as system_views
from lexicons import views as lexicons_views

router = DefaultRouter()
router.register(r'user', system_views.UserViewSet)
router.register(r'audithistory', audit_views.AuditHistoryViewSet)
router.register(r'basemodel', crux_views.BaseModelViewSet)
router.register(r'datatypes', crux_views.DataTypesViewSet)
router.register(r'listfilter', crux_views.ListFilterViewSet)
router.register(r'list', crux_views.ListViewSet)
router.register(r'listorstring', crux_views.ListOrStringViewSet)
router.register(r'measurements', crux_views.MeasurementsViewSet)
router.register(r'listitemcodes', crux_views.ListItemCodesViewSet)
router.register(r'listitemquantitativesettings', crux_views.ListItemQuantitativeSettingsViewSet)
router.register(r'listitem', crux_views.ListItemViewSet)
router.register(r'listitemattributes', crux_views.ListItemAttributesViewSet)
router.register(r'listitemtriggers', crux_views.ListItemTriggersViewSet)
router.register(r'underlayment', crux_views.UnderlaymentViewSet)
router.register(r'outlineunderlayment', crux_views.OutlineUnderlaymentViewSet)
router.register(r'adjectives', crux_views.AdjectivesViewSet)
router.register(r'outlineattributes', crux_views.OutlineAttributesViewSet)
router.register(r'outline', crux_views.OutlineViewSet)
router.register(r'outlineitemattributes', crux_views.OutlineItemAttributesViewSet)
router.register(r'outlineitem', crux_views.OutlineItemViewSet)
router.register(r'encountertimer', encounters_views.EncounterTimerViewSet)
router.register(r'encounterattributes', encounters_views.EncounterAttributesViewSet)
router.register(r'encounterrelationship', encounters_views.EncounterRelationshipViewSet)
router.register(r'encounterdate', encounters_views.EncounterDateViewSet)
router.register(r'encounterentity', encounters_views.EncounterEntityViewSet)
router.register(r'encounterunderlayment', encounters_views.EncounterUnderlaymentViewSet)
router.register(r'encounter', encounters_views.EncounterViewSet)
router.register(r'episodeentity', encounters_views.EpisodeEntityViewSet)
router.register(r'episodeattributes', encounters_views.EpisodeAttributesViewSet)
router.register(r'episodedate', encounters_views.EpisodeDateViewSet)
router.register(r'episode', encounters_views.EpisodeViewSet)
router.register(r'episodestage', encounters_views.EpisodeStageViewSet)
router.register(r'detailattributes', encounters_views.DetailAttributesViewSet)
router.register(r'detailmeasurements', encounters_views.DetailMeasurementsViewSet)
router.register(r'detailquantitativemeasurements', encounters_views.DetailQuantitativeMeasurementsViewSet)
router.register(r'detailquantitativevalues', encounters_views.DetailQuantitativeValuesViewSet)
router.register(r'detailcodes', encounters_views.DetailCodesViewSet)
router.register(r'detail', encounters_views.DetailViewSet)
router.register(r'address', entities_views.AddressViewSet)
router.register(r'language', entities_views.LanguageViewSet)
router.register(r'phone', entities_views.PhoneViewSet)
router.register(r'relationship', entities_views.RelationshipViewSet)
router.register(r'entityattributes', entities_views.EntityAttributesViewSet)
router.register(r'entity', entities_views.EntityViewSet)
router.register(r'entityparameter', entities_views.EntityParameterViewSet)
router.register(r'entitysectionsusers', entities_views.EntitySectionsUsersViewSet)
router.register(r'entitysectionsattributes', entities_views.EntitySectionsAttributesViewSet)
router.register(r'entitysections', entities_views.EntitySectionsViewSet)
router.register(r'lexicon', lexicons_views.LexiconViewSet)
router.register(r'lexindex', lexicons_views.LexIndexViewSet)
router.register(r'corporaattributes', lexicons_views.CorporaAttributesViewSet)
router.register(r'corpora', lexicons_views.CorporaViewSet)
router.register(r'extendeduserinformationattributes', system_views.ExtendedUserInformationAttributesViewSet)
router.register(r'rights', system_views.RightsViewSet)
router.register(r'extendeduserinformation', system_views.ExtendedUserInformationViewSet)
router.register(r'parameterscodes', system_views.ParametersCodesViewSet)
router.register(r'parameterattributes', system_views.ParameterAttributesViewSet)
router.register(r'parameters', system_views.ParametersViewSet)
router.register(r'location', system_views.LocationViewSet)
router.register(r'settings', system_views.SettingsViewSet)
router.register(r'entityparameterdefinition', system_views.EntityParameterDefinitionViewSet)

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
    url(r'^api/v1/importer/$', system_views.importer),
    url(r'^api/v1/importer$', system_views.importer),
    url(r'^api/v1/import/$', system_views.start_import),
    url(r'^api/v1/import$', system_views.start_import),
    url(r'^api/v1/token/', system_views.obtain_auth_token),
    url(r'^api/v1/token$', system_views.obtain_auth_token),
    url(r'^api/v1/', include(router.urls)),
    url(r'^login/', auth_views.login, {'template_name': 'auth/login.html'}),
    url(r'^login$', auth_views.login, {'template_name': 'auth/login.html'}),
    url(r'^logout/', auth_views.logout, {'template_name': 'auth/logged_out.html'}),
    url(r'^logout$', auth_views.logout, {'template_name': 'auth/logged_out.html'}),
    url(r'^reset/password/', auth_views.password_reset, name='password_reset'),
    url(r'^reset/password$', auth_views.password_reset),
    url(r'^reset/password/complete/', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^admin', include(admin.site.urls)),
    url(r'^$', login_required(system_views.index)),
]