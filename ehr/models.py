import hashlib
import uuid

from django.contrib.auth.models import User
from django.db import models

#
#class BaseModel(models.Model):
#    """
#    Base model for all other models
#    """
#    @staticmethod
#    def uuid():
#        m = hashlib.md5()
#        m.update(str(uuid.uuid1())+str(uuid.uuid4())+str(uuid.uuid5(uuid.uuid1(),str(uuid.uuid4()))))
#        return str(uuid.uuid5(uuid.uuid4(), m.hexdigest()))
#
#    export_id = models.CharField(max_length=36, db_index=True, default=uuid)
#
#
#class Address(BaseModel):
#    """
#    Addresses for ${Entity} records
#    """
#    entity = models.ForeignKey("Entity")
#    type = models.CharField(max_length=50, default="Primary")
#    primary_line = models.CharField(max_length=50, null=True, blank=True)
#    secondary_line = models.CharField(max_length=50, null=True, blank=True)
#    city = models.CharField(max_length=50, null=True, blank=True)
#    state = models.CharField(max_length=50, null=True, blank=True)
#    country = models.CharField(max_length=50, null=True, blank=True)
#    zip = models.CharField(max_length=10, null=True, blank=True)
#
#
#class Language(BaseModel):
#    """
#    Languages for ${Entity} records
#    """
#    entity = models.ForeignKey("Entity")
#    type = models.CharField(max_length=50, default="Primary")
#    language = models.CharField(max_length=50)
#
#
#class Phone(BaseModel):
#    """
#    Phones for ${Entity} records
#    """
#    entity = models.ForeignKey("Entity")
#    type = models.CharField(max_length=50, default="Primary")
#    phone = models.CharField(max_length=50, null=True, blank=True)
#
#
#class Relationship(BaseModel):
#    """
#    Relationships between ${Entity} records
#    """
#    entity = models.ForeignKey("Entity", related_name="entities")
#    relation = models.ForeignKey("Entity", related_name="relations")
#    type = models.CharField(max_length=50, default="Related", db_index=True)
#
#
#class SystemListItemTriggers(BaseModel):
#    """
#    Triggers for ${SystemListItem} records
#    """
#    item = models.ForeignKey("SystemListItem", related_name="triggers")
#    type = models.CharField(max_length=50, default="None", db_index=True)
#    include_modifiers = models.CharField(max_length=1, null=True, blank=True)
#
#
#
#class ListItemTriggers(BaseModel):
#    """
#    Triggers for ${ListItem} records
#    """
#    item = models.ForeignKey("ListItem", related_name="triggers")
#    type = models.CharField(max_length=50, default="None", db_index=True)
#    include_modifiers = models.CharField(max_length=1, null=True, blank=True)
#
#
#class ExtendedUserInformation(BaseModel):
##AllUsers
#    """
#    Extended User Information
#    """
#    #id >> export_id
#    user = models.ForeignKey(User, default='')
#    last_name = models.CharField(max_length=50, null=True, blank=True)
#    first_name = models.CharField(max_length=50, null=True, blank=True)
#    #mi
#    middle_name = models.CharField(max_length=50, null=True, blank=True)
#    title = models.CharField(max_length=50, null=True, blank=True)
#    #lic_no
#    license_number = models.CharField(max_length=50, null=True, blank=True)
#    #dea
#    dea_number = models.CharField(max_length=50, null=True, blank=True)
#    #emp_no
#    employee_number = models.CharField(max_length=50, null=True, blank=True)
#    #insvc_date
#    in_service_date = models.DateTimeField(null=True, blank=True)
#    #osvc_date
#    out_of_service_date = models.DateTimeField(null=True, blank=True)
#    #magic
#    indicia = models.IntegerField(null=True, blank=True)
#    role = models.CharField(max_length=50, null=True, blank=True)
#    #cw
#    classification = models.CharField(max_length=1, null=True, blank=True)
#    #chartp
#    can_chart = models.CharField(max_length=1, null=True, blank=True)
#    #refillp
#    can_refill = models.CharField(max_length=1, null=True, blank=True)
#    #viewp
#    can_view = models.CharField(max_length=1, null=True, blank=True)
#    #enrollp
#    can_enroll = models.CharField(max_length=1, null=True, blank=True)
#    #transp
#    can_transcribe = models.CharField(max_length=1, null=True, blank=True)
#    #cosignp
#    can_cosign = models.CharField(max_length=1, null=True, blank=True)
#    #active
#    is_active = models.CharField(max_length=1, null=True, blank=True)
#    #adminp
#    is_admin = models.CharField(max_length=1, null=True, blank=True)
#    service = models.CharField(max_length=50, null=True, blank=True)
#    team = models.CharField(max_length=50, null=True, blank=True)
#    nickname = models.CharField(max_length=36, null=True, blank=True)
#    create_date = models.DateTimeField(null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    pin = models.CharField(max_length=50, null=True, blank=True)
#    #passcode
#    pass_code = models.CharField(max_length=50, null=True, blank=True)
#    #pwd_set_date
#    pass_code_set_date = models.DateTimeField(null=True, blank=True)
#    #pwd_expire_date
#    pass_code_expiration_date = models.DateTimeField(null=True, blank=True)
#    #pwd_change_reqd
#    pass_code_change_is_required = models.CharField(max_length=1, null=True, blank=True)
#    #pwd_never_expires
#    pass_code_never_expires = models.CharField(max_length=1, null=True, blank=True)
#    #create_by
#    created_by = models.ForeignKey(User)
#    #change_by
#    changed_by = models.ForeignKey(User)
#    phone = models.CharField(max_length=50, null=True, blank=True)
#    cellphone = models.CharField(max_length=50, null=True, blank=True)
#    fax = models.CharField(max_length=50, null=True, blank=True)
#    pager = models.CharField(max_length=50, null=True, blank=True)
#    email = models.CharField(max_length=50, null=True, blank=True)
#
#
#class Encounters(BaseModel):
##EventMaster
#    """
#    Clinical and Non-clinical encounter records
#    """
#    #event_id >> export_id
#    #pid
#    entity = models.ForeignKey(Entity, related_name="events")
#    #prior_event_link
#    prior_encounter = models.ForeignKey("Encounters", related_name="next_encounter", null=True, blank=True)
#    #event_type
#    type = models.CharField(max_length=50, null=True, blank=True)
#    record_type = models.CharField(max_length=50, null=True, blank=True)
#    document_title = models.CharField(max_length=75, null=True, blank=True)
#    document_type = models.CharField(max_length=50, null=True, blank=True)
#    #rev_level
#    revision_level = models.CharField(max_length=50, null=True, blank=True)
#    link_type = models.CharField(max_length=50, null=True, blank=True)
#    order_id = models.CharField(max_length=36, null=True, blank=True)
#    ordered_by = models.ForeignKey(ExtendedUserInformation, null=True, blank=True, related_name="encounters_ordered")
#    #performedby_id
#    performed_by = models.ForeignKey(ExtendedUserInformation, null=True, blank=True,
#                                     related_name="encounters_performed")
#    #performed_by field removed redundant
#    request_date = models.DateTimeField(null=True, blank=True)
#    #event_date
#    date = models.DateTimeField(null=True, blank=True)
#    #event_loc
#    location = models.CharField(max_length=36, null=True, blank=True)
#    system_date = models.DateTimeField(null=True, blank=True)
#    #event
#    corpus = models.TextField(null=True, blank=True)
#    #interpretedby_id
#    interpreted_by = models.ForeignKey(ExtendedUserInformation, null=True, blank=True,
#                                       related_name="encounters_interpreted")
#    interpreted_date = models.DateTimeField(null=True, blank=True)
#    #interpreted_by field removed redundant
#    priority = models.CharField(max_length=10, null=True, blank=True)
#    #event_completion_status
#    completion_status = models.CharField(max_length=10, null=True, blank=True)
#    lab_section_id = models.CharField(max_length=10, null=True, blank=True)
#    action_code = models.CharField(max_length=10, null=True, blank=True)
#    #relevant_clin_info
#    relevant_clinical_info = models.CharField(max_length=255, null=True, blank=True)
#    reason = models.CharField(max_length=255, null=True, blank=True)
#    completion_date = models.DateTimeField(null=True, blank=True)
#    #org
#    organization = models.CharField(max_length=50, null=True, blank=True)
#    #dept
#    department = models.CharField(max_length=50, null=True, blank=True)
#    forward_link = models.CharField(max_length=36, null=True, blank=True)
#    age_at_event = models.IntegerField(null=True, blank=True)
#    restrictions = models.CharField(max_length=255, null=True, blank=True)
#    utc_bias = models.IntegerField(null=True, blank=True)
#    transcribed_by = models.ForeignKey(ExtendedUserInformation, null=True, blank=True,
#                                       related_name="encounters_transcribed")
#    transcribed_time = models.DateTimeField(null=True, blank=True)
#    transcribed_org = models.CharField(max_length=36, null=True, blank=True)
#    hash = models.CharField(max_length=50, null=True, blank=True)
#    #episodeid
#    episode = models.ForeignKey(Episode, related_name="encounters", null=True, blank=True)
#    #episodeseq
#    episode_sequence = models.IntegerField(null=True, blank=True)
#    renderer = models.CharField(max_length=255, null=True, blank=True)
#    #reviewedby_id
#    reviewed_by = models.ForeignKey(ExtendedUserInformation, null=True, blank=True, related_name="events_reviewed")
#    #reviewed_by field removed redundant
#    reviewed_date = models.DateTimeField(null=True, blank=True)
#    #evdocloc maps to service_location.loc_code then maps out the service location id
#    service_location = models.ForeignKey(ServiceLocations, related_name="encounters", null=True, blank=True)
#    #page1uid
#    first_page_underlayment = models.ForeignKey(DocUnderlayment, related_name="encounters_first_page", null=True,
#                                                blank=True)
#    #pageevenuid
#    even_page_underlayment = models.ForeignKey(DocUnderlayment, related_name="encounters_first_page", null=True,
#                                               blank=True)
#    #pageodduid
#    odd_page_underlayment = models.ForeignKey(DocUnderlayment, related_name="encounters_first_page", null=True,
#                                              blank=True)
#    encounter_time = models.IntegerField(null=True, blank=True)
#    face_to_face_time = models.IntegerField(null=True, blank=True)
#
#
#class Attributes(BaseModel):
#    """
#    Attributes for an ${Entity} record
#    """
#    entity = models.ForeignKey(Entity, related_name="attributes", db_index=True)
#    type = models.CharField(max_length=50, db_index=True)
#    value = models.CharField(max_length=255)
#    searchable = models.CharField(max_length=1, default="Y")
#
#
#class Entity(BaseModel):
#    """
#    People, Organizations, or other legal entities
#    """
#    #pid >> export_id
#    #mrn
#    record_number = models.CharField(max_length=50, null=True, blank=True, db_index=True)
#    #fname >> attributes record with type of "first" unless is_org = Y then names record with type of "name"
#    #mname >> attributes record with type of "middle"
#    #lname >> attributes record with type of "last"
#    #prefix >> attributes record with type of "prefix"
#    #suffix >> attributes record with type of "suffix"
#    #degree >> attributes record with type of "degree"
#    #name_type_code >> attributes record with type of "name_type_code"
#    #birthdate
#    start_date = models.DateTimeField(null=True, blank=True, db_index=True)
#    #deathdate
#    end_date = models.DateTimeField(null=True, blank=True, db_index=True)
#
#    #sex >> attributes record with type of "gender"
#
#    # addr1 -> address.primary_line
#    # addr2 -> address.secondary_line
#    # city -> address.city
#    # state -> address.state
#    # country -> address.country
#    # zip -> address.zip
#
#    # homephone -> homephone.phone
#    # workphone -> workphone.phone
#
#    #ssn >> attributes record with type of "ssn" or "eid" if is_org = Y
#
#    #ru_id + relat_to_ru -> relationship
#    #mother_id + "Mother" -> relationship
#    #father_id + "Father" -> relationship
#    #guar_id + "Guarantor" -> relationship
#
#    #is_pt
#    encounterable = models.CharField(max_length=1, null=True, blank=True)
#
#    #is_ru >> attributes record with type of "is_residential_unit_head"
#
#    #is_org >> attributes record with type of "is_organization"
#
#    #lwr >> attributes record with type of "lives_with_relationship"
#
#    remark = models.TextField(null=True, blank=True)
#
#    created = models.DateTimeField(null=True, blank=True)
#    created_by = models.ForeignKey(ExtendedUserInformation, null=True, blank=True, related_name="entities_created")
#
#    #date_changed -> updated
#    updated = models.DateTimeField(null=True, blank=True)
#    updated_by = models.ForeignKey(ExtendedUserInformation, null=True, blank=True, related_name="entities_updated")
#
#    #last_visit
#    last_encounter = models.ForeignKey(Encounters, null=True, blank=True)
#
#    #race >> attributes record with type of "race"
#
#    #preferred_lang -> language record
#
##Sequence Dropped as useless
#
#
#class Alerts(BaseModel):
##Allergies
#    """
#    Alerts for ${Entity} records
#    """
#    #allerg_id >> export_id
#    #pid
#    entity = models.ForeignKey(Entity, related_name="alerts")
#    #allergy
#    alert = models.CharField(max_length=255, null=True, blank=True)
#    remark = models.TextField(null=True, blank=True)
#    coded_value = models.CharField(max_length=20, null=True, blank=True)
#    #code_src
#    code_source = models.CharField(max_length=10, null=True, blank=True)
#    #examiner_id
#    creator = models.ForeignKey(ExtendedUserInformation, null=True, blank=True)
#    #attending_id
#    supervisor = models.ForeignKey(ExtendedUserInformation, null=True, blank=True)
#    active_date = models.DateTimeField(null=True, blank=True)
#    inactive_date = models.DateTimeField(null=True, blank=True)
#    system_date = models.DateTimeField(null=True, blank=True)
#    priority = models.CharField(max_length=10, null=True, blank=True)
#    #allerg_status
#    status = models.CharField(max_length=10, null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    #episodeid
#    episode = models.ForeignKey(Episode, null=True, blank=True, related_name="alerts")
#
##AppLaunchItems Dropped as useless
#
#
#class SystemListItem(BaseModel):
##AppMainDict
#    #id >> export_id
#    snippet = models.ForeignKey("SystemList", related_name="items")
#    #code_group defined by snippet record
#    #code_group = models.CharField(max_length=30, null=True, blank=True)
#    #code_clas defined by snippet record
#    #code_class = models.CharField(max_length=30, null=True, blank=True)
#    fact_code = models.CharField(max_length=30, null=True, blank=True)
#    norm_flag = models.CharField(max_length=5, null=True, blank=True)
#    #stext
#    summary_value = models.CharField(max_length=255, null=True, blank=True)
#    sort_seq = models.IntegerField(null=True, blank=True)
#    modifiers = models.CharField(max_length=240, null=True, blank=True)
#    #includeif
#    inclusion_clause = models.CharField(max_length=50, null=True, blank=True)
#    num_copies = models.FloatField(null=True, blank=True)
#    source = models.CharField(max_length=10, null=True, blank=True)
#    std_code = models.CharField(max_length=50, null=True, blank=True)
#    units = models.CharField(max_length=50, null=True, blank=True)
#    site = models.CharField(max_length=50, null=True, blank=True)
#    method = models.CharField(max_length=50, null=True, blank=True)
#    text_as_section = models.CharField(max_length=1, null=True, blank=True)
#    frequency = models.CharField(max_length=50, null=True, blank=True)
#    duration = models.CharField(max_length=50, null=True, blank=True)
#    strength = models.CharField(max_length=50, null=True, blank=True)
#    lot_num = models.CharField(max_length=5, null=True, blank=True)
#    #dunits
#    datum_units = models.CharField(max_length=50, null=True, blank=True)
#    #dsite
#    datum_site = models.CharField(max_length=50, null=True, blank=True)
#    #dmethod
#    datum_method = models.CharField(max_length=50, null=True, blank=True)
#    #dfreq
#    datum_frequency = models.CharField(max_length=50, null=True, blank=True)
#    #ddur
#    datum_duration = models.CharField(max_length=50, null=True, blank=True)
#    #dstren
#    datum_strength = models.CharField(max_length=50, null=True, blank=True)
#    destination = models.CharField(max_length=50, null=True, blank=True)
#    num_values = models.IntegerField(null=True, blank=True)
#    #dval1
#    datum_value = models.CharField(max_length=50, null=True, blank=True)
#    #qmod
#    quantitative_modifier = models.ForeignKey("SystemList", related_name="connections")
#    #qmod_promote
#    quantitative_promotion = models.CharField(max_length=1, null=True, blank=True)
#    create_date = models.DateTimeField(null=True, blank=True)
#    foundation = models.CharField(max_length=1, null=True, blank=True)
#    auto_prob = models.CharField(max_length=1, null=True, blank=True)
#    auto_dx = models.CharField(max_length=1, null=True, blank=True)
#    inc_dx_mods = models.CharField(max_length=1, null=True, blank=True)
#    auto_track = models.CharField(max_length=1, null=True, blank=True)
#    #d_trackact
#    tracking_action = models.CharField(max_length=50, null=True, blank=True)
#    #d_tracktime
#    tracking_time = models.IntegerField(null=True, blank=True)
#    #d_trackunit
#    tracking_unit = models.CharField(max_length=10, null=True, blank=True)
#    #auto_curmed tracked in an associated SystemListItemTrigger
#    sem_code = models.CharField(max_length=50, null=True, blank=True)
#    udt = models.CharField(max_length=50, null=True, blank=True)
#    rule = models.CharField(max_length=50, null=True, blank=True)
#    #pneg
#    negative_value = models.CharField(max_length=128, null=True, blank=True)
#    #full_text
#    value = models.TextField(null=True, blank=True)
#    #reqd
#    is_required = models.CharField(max_length=1, null=True, blank=True)
#    restrictions = models.CharField(max_length=128, null=True, blank=True)
#    result_data_type = models.CharField(max_length=10, null=True, blank=True)
#    #quant_data_type
#    quantitative_type = models.CharField(max_length=10, null=True, blank=True)
#    fact_code_data_type = models.CharField(max_length=10, null=True, blank=True)
#    auto_param = models.CharField(max_length=1, null=True, blank=True)
#    xml_export_tag_name = models.CharField(max_length=50, null=True, blank=True)
#    edit_date = models.DateTimeField(null=True, blank=True)
#    #qlaunch_before
#    before_quantitative_launch_action = models.CharField(max_length=50, null=True, blank=True)
#    #qlaunch_after
#    after_quantitative_launch_action = models.CharField(max_length=50, null=True, blank=True)
#    launch_group = models.CharField(max_length=50, null=True, blank=True)
#
#
#class SystemList(BaseModel):
##AppSnippets
#    #snippet_id >> export_id
#    code_class = models.CharField(max_length=30, null=True, blank=True)
#    code_group = models.CharField(max_length=30, null=True, blank=True)
#    definition = models.CharField(max_length=255, null=True, blank=True)
#    mode = models.CharField(max_length=10, null=True, blank=True)
#    num_ret_val = models.IntegerField(null=True, blank=True)
#    corpus = models.TextField(null=True, blank=True)
#    create_date = models.DateTimeField(null=True, blank=True)
#    edit_date = models.DateTimeField(null=True, blank=True)
#
#
#class AuditHistory(BaseModel):
#    #pid
#    entity = models.ForeignKey(Entity, related_name="audit_history", null=True, blank=True)
#    #tablename
#    table_name = models.CharField(max_length=250)
#    #changedate
#    change_date = models.DateTimeField()
#    #changebyid
#    change_by = models.ForeignKey(ExtendedUserInformation, related_name="changes")
#    #changetype
#    change_type = models.CharField(max_length=250)
#
#
#class CurMeds(BaseModel):
#    med_id = models.CharField(max_length=36)
#    pid = models.ForeignKey(Entity, related_name="curmeds", null=True, blank=True)
#    event_id = models.ForeignKey(Encounters, related_name="curmeds", null=True, blank=True)
#    item_category = models.CharField(max_length=50, null=True, blank=True)
#    item_name = models.CharField(max_length=50, null=True, blank=True)
#    item_value = models.CharField(max_length=255, null=True, blank=True)
#    item_comment = models.TextField(null=True, blank=True)
#    action_taken = models.CharField(max_length=50, null=True, blank=True)
#    status_code = models.CharField(max_length=50, null=True, blank=True)
#    coded_value = models.CharField(max_length=20, null=True, blank=True)
#    code_src = models.CharField(max_length=10, null=True, blank=True)
#    start_date = models.DateTimeField(null=True, blank=True)
#    startby_id = models.ForeignKey(ExtendedUserInformation, related_name="medications_started", null=True, blank=True)
#    refill_date = models.DateTimeField(null=True, blank=True)
#    refillby_id = models.ForeignKey(ExtendedUserInformation, related_name="medications_refilled", null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    #upd_date
#    updated_date = models.DateTimeField(null=True, blank=True)
#    #updby_id
#    updatedby_id = models.ForeignKey(ExtendedUserInformation, related_name="medications_updated", null=True, blank=True)
#    system_date = models.DateTimeField(null=True, blank=True)
#    episodeid = models.ForeignKey(Episode, related_name="curmeds", null=True, blank=True)
#
#
#class Settings(BaseModel):
##CwSettings
#    userid = models.ForeignKey(ExtendedUserInformation, related_name="settings")
#    settingname = models.CharField(max_length=36)
#    settingvalue = models.TextField(null=True, blank=True)
#    usesystem = models.CharField(max_length=1, null=True, blank=True)
#
#
#class ChargeQueue(BaseModel):
#    event_id = models.ForeignKey(Encounters, related_name="charges")
#
#
#class DbControl(BaseModel):
#    build_num = models.IntegerField()
#    updated = models.DateTimeField(null=True, blank=True)
#    dbclass = models.CharField(max_length=50)
#    minorbuildnum = models.IntegerField()
#
#
#class DocTypes(BaseModel):
#    set_name = models.CharField(max_length=50, null=True, blank=True)
#    rev_level = models.CharField(max_length=5, null=True, blank=True)
#    description = models.CharField(max_length=50, null=True, blank=True)
#    record_type = models.CharField(max_length=50, null=True, blank=True)
#    normal_key = models.CharField(max_length=50, null=True, blank=True)
#    in_use = models.CharField(max_length=1, null=True, blank=True)
#    check2 = models.CharField(max_length=5, null=True, blank=True)
#    create_date = models.DateTimeField(null=True, blank=True)
#    dtd_id = models.CharField(max_length=36, primary_key=True)
#    require_dx = models.CharField(max_length=1, null=True, blank=True)
#    auto_expand = models.CharField(max_length=1, null=True, blank=True)
#    sort_seq = models.IntegerField(null=True, blank=True)
#    copies = models.IntegerField(null=True, blank=True)
#    d_reason = models.CharField(max_length=50, null=True, blank=True)
#    definition = models.CharField(max_length=255, null=True, blank=True)
#    cust_hdr = models.TextField(null=True, blank=True)
#    cust_ftr = models.TextField(null=True, blank=True)
#    ucase_abn = models.CharField(max_length=1, null=True, blank=True)
#    reqd = models.CharField(max_length=1, null=True, blank=True)
#    compile_date = models.DateTimeField(null=True, blank=True)
#    compile_path = models.CharField(max_length=128, null=True, blank=True)
#    restrictions = models.CharField(max_length=128, null=True, blank=True)
#    reqcsig = models.CharField(max_length=1, null=True, blank=True)
#    line_below = models.CharField(max_length=1, null=True, blank=True)
#    line_above = models.CharField(max_length=1, null=True, blank=True)
#    first_line = models.TextField(null=True, blank=True)
#    use_rtf = models.CharField(max_length=50, null=True, blank=True)
#    cw = models.IntegerField(null=True, blank=True)
#    post_launch = models.CharField(max_length=1000, null=True, blank=True)
#    custom_exit = models.CharField(max_length=1000, null=True, blank=True)
#    save_renderer = models.CharField(max_length=1, null=True, blank=True)
#    retain_formatting = models.CharField(max_length=1, null=True, blank=True)
#    save_headers = models.CharField(max_length=1, null=True, blank=True)
#    save_params = models.CharField(max_length=1, null=True, blank=True)
#    episodeseq = models.IntegerField(null=True, blank=True)
#    auto_queue = models.CharField(max_length=1, null=True, blank=True)
#    trigger_episode_printing = models.CharField(max_length=1, null=True, blank=True)
#    left_margin = models.CharField(max_length=10, null=True, blank=True)
#    right_margin = models.CharField(max_length=10, null=True, blank=True)
#    top_margin = models.CharField(max_length=10, null=True, blank=True)
#    bottom_margin = models.CharField(max_length=10, null=True, blank=True)
#    footerfontsize = models.CharField(max_length=10, null=True, blank=True)
#    bodyfontsize = models.CharField(max_length=10, null=True, blank=True)
#    use_custom_headers = models.CharField(max_length=1, null=True, blank=True)
#    use_custom_layout = models.CharField(max_length=1, null=True, blank=True)
#    page1uid = models.CharField(max_length=36, null=True, blank=True)
#    pageevenuid = models.CharField(max_length=36, null=True, blank=True)
#    pageodduid = models.CharField(max_length=36, null=True, blank=True)
#
#
#class DocUnderlayment(BaseModel):
#    uid = models.CharField(max_length=36)
#    name = models.CharField(max_length=50)
#    description = models.TextField(null=True, blank=True)
#    bits = models.TextField()
#    available = models.CharField(max_length=1)
#
#
#class EntityParam(BaseModel):
#    pid = models.CharField(max_length=36)
#    p_name = models.CharField(max_length=50)
#    p_value = models.CharField(max_length=255, null=True, blank=True)
#    p_data_type = models.CharField(max_length=10, null=True, blank=True)
#    p_units = models.CharField(max_length=50, null=True, blank=True)
#    p_date_value = models.DateTimeField(null=True, blank=True)
#    p_fact_code = models.CharField(max_length=50, null=True, blank=True)
#    p_fact_code_data_type = models.CharField(max_length=10, null=True, blank=True)
#    p_quant_fact_code = models.CharField(max_length=50, null=True, blank=True)
#    p_quant_fact_code_data_type = models.CharField(max_length=10, null=True, blank=True)
#    p_coded_value = models.CharField(max_length=20, null=True, blank=True)
#    p_code_src = models.CharField(max_length=10, null=True, blank=True)
#    p_semantic_code = models.CharField(max_length=50, null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    acl = models.CharField(max_length=255, null=True, blank=True)
#    p_return = models.CharField(max_length=1, null=True, blank=True)
#
#
#class EntityParamDefs(BaseModel):
#    p_name = models.CharField(max_length=50)
#    p_data_type = models.CharField(max_length=10, null=True, blank=True)
#    p_default_value = models.CharField(max_length=50, null=True, blank=True)
#    p_query = models.CharField(max_length=255, null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    p_return = models.CharField(max_length=1, null=True, blank=True)
#
#
#class Episode(BaseModel):
#    episodeid = models.CharField(max_length=36)
#    pid = models.CharField(max_length=36, null=True, blank=True)
#    facility = models.CharField(max_length=50, null=True, blank=True)
#    area = models.CharField(max_length=50, null=True, blank=True)
#    activitycode = models.CharField(max_length=50, null=True, blank=True)
#    to_see = models.CharField(max_length=36, null=True, blank=True)
#    episodetype = models.CharField(max_length=50, null=True, blank=True)
#    arrivetime = models.DateTimeField(null=True, blank=True)
#    arrivelocation = models.CharField(max_length=36, null=True, blank=True)
#    departtime = models.DateTimeField(null=True, blank=True)
#    departlocation = models.CharField(max_length=36, null=True, blank=True)
#    episodeclasscode = models.CharField(max_length=50, null=True, blank=True)
#    chiefcomplaint = models.CharField(max_length=255, null=True, blank=True)
#    ptage = models.CharField(max_length=50, null=True, blank=True)
#    camefrom = models.CharField(max_length=50, null=True, blank=True)
#    sendingfacilityname = models.CharField(max_length=50, null=True, blank=True)
#    transmode = models.CharField(max_length=50, null=True, blank=True)
#    ambulanceservice = models.CharField(max_length=50, null=True, blank=True)
#    squad = models.CharField(max_length=50, null=True, blank=True)
#    crimevictim = models.CharField(max_length=50, null=True, blank=True)
#    policereportrequired = models.CharField(max_length=50, null=True, blank=True)
#    policeresponsedate = models.DateTimeField(null=True, blank=True)
#    policeofficer = models.CharField(max_length=50, null=True, blank=True)
#    signinby = models.CharField(max_length=36, null=True, blank=True)
#    priority = models.CharField(max_length=50, null=True, blank=True)
#    pttype = models.CharField(max_length=50, null=True, blank=True)
#    broughtby = models.CharField(max_length=50, null=True, blank=True)
#    localcontactphone = models.CharField(max_length=50, null=True, blank=True)
#    disposition = models.CharField(max_length=50, null=True, blank=True)
#    destinationfacilityname = models.CharField(max_length=50, null=True, blank=True)
#    followupapptreqd = models.CharField(max_length=50, null=True, blank=True)
#    signoutby = models.CharField(max_length=36, null=True, blank=True)
#    referredto = models.CharField(max_length=50, null=True, blank=True)
#    accountnumber = models.CharField(max_length=50, null=True, blank=True)
#    episodecomplete = models.CharField(max_length=1, null=True, blank=True)
#    reglname = models.CharField(max_length=50, null=True, blank=True)
#    regfname = models.CharField(max_length=50, null=True, blank=True)
#    systemdate = models.DateTimeField(null=True, blank=True)
#    controlnumber = models.CharField(max_length=36, null=True, blank=True)
#    authorizationinfo = models.CharField(max_length=100, null=True, blank=True)
#    priorlinkedepisodeid = models.CharField(max_length=36, null=True, blank=True)
#    nextlinkedepisodeid = models.CharField(max_length=36, null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#
#
#class EpisodeDx(BaseModel):
#    detail_id = models.CharField(max_length=36)
#    event_id = models.CharField(max_length=36, null=True, blank=True)
#    episodeid = models.CharField(max_length=36, null=True, blank=True)
#    pid = models.CharField(max_length=36, null=True, blank=True)
#    dxrank = models.IntegerField(null=True, blank=True)
#    dxdesc = models.CharField(max_length=255, null=True, blank=True)
#    dxcode = models.CharField(max_length=20, null=True, blank=True)
#    dxsrc = models.CharField(max_length=10, null=True, blank=True)
#    has_detail = models.CharField(max_length=1, null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    create_date = models.DateTimeField(null=True, blank=True)
#
#
#class EpisodePr(BaseModel):
#    detail_id = models.CharField(max_length=36)
#    event_id = models.CharField(max_length=36, null=True, blank=True)
#    episodeid = models.CharField(max_length=36, null=True, blank=True)
#    episodeseq = models.IntegerField(null=True, blank=True)
#    prcode = models.CharField(max_length=20, null=True, blank=True)
#    prsrc = models.CharField(max_length=10, null=True, blank=True)
#    dxcode_1 = models.CharField(max_length=20, null=True, blank=True)
#    dxsrc_1 = models.CharField(max_length=10, null=True, blank=True)
#    dxcode_2 = models.CharField(max_length=20, null=True, blank=True)
#    dxsrc_2 = models.CharField(max_length=10, null=True, blank=True)
#    dxcode_3 = models.CharField(max_length=20, null=True, blank=True)
#    dxsrc_3 = models.CharField(max_length=10, null=True, blank=True)
#    dxcode_4 = models.CharField(max_length=20, null=True, blank=True)
#    dxsrc_4 = models.CharField(max_length=10, null=True, blank=True)
#    isproc = models.CharField(max_length=1, null=True, blank=True)
#    procmodifiers = models.CharField(max_length=50, null=True, blank=True)
#
#
#class EpisodeRegister(BaseModel):
#    episodeid = models.CharField(max_length=36)
#    currentlocationtime = models.DateTimeField(null=True, blank=True)
#    currentlocation = models.CharField(max_length=36, null=True, blank=True)
#    systemdate = models.DateTimeField(null=True, blank=True)
#    status = models.CharField(max_length=50, null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    nextstage = models.CharField(max_length=50, null=True, blank=True)
#
#
#class EventDetail(BaseModel):
#    event_id = models.CharField(max_length=36)
#    detail_id = models.CharField(max_length=36)
#    pid = models.CharField(max_length=36, null=True, blank=True)
#    specimen_source = models.CharField(max_length=50, null=True, blank=True)
#    result_group = models.CharField(max_length=100, null=True, blank=True)
#    result_category = models.CharField(max_length=100, null=True, blank=True)
#    result_name = models.CharField(max_length=50, null=True, blank=True)
#    result = models.CharField(max_length=255, null=True, blank=True)
#    result_data_type = models.CharField(max_length=10, null=True, blank=True)
#    remark = models.TextField(null=True, blank=True)
#    quant_value = models.CharField(max_length=255, null=True, blank=True)
#    quant_value_data_type = models.CharField(max_length=10, null=True, blank=True)
#    units = models.CharField(max_length=50, null=True, blank=True)
#    site = models.CharField(max_length=50, null=True, blank=True)
#    method = models.CharField(max_length=50, null=True, blank=True)
#    frequency = models.CharField(max_length=50, null=True, blank=True)
#    duration = models.CharField(max_length=50, null=True, blank=True)
#    strength = models.CharField(max_length=50, null=True, blank=True)
#    lot_num = models.CharField(max_length=50, null=True, blank=True)
#    coded_value = models.CharField(max_length=20, null=True, blank=True)
#    code_src = models.CharField(max_length=10, null=True, blank=True)
#    abnormal_flag = models.CharField(max_length=10, null=True, blank=True)
#    status_code = models.CharField(max_length=10, null=True, blank=True)
#    prob_link = models.CharField(max_length=255, null=True, blank=True)
#    dx_flag = models.CharField(max_length=1, null=True, blank=True)
#    action_taken = models.CharField(max_length=50, null=True, blank=True)
#    quant_date = models.DateTimeField(null=True, blank=True)
#    sem_code = models.CharField(max_length=50, null=True, blank=True)
#    fact_code = models.CharField(max_length=50, null=True, blank=True)
#    fact_code_data_type = models.CharField(max_length=10, null=True, blank=True)
#    quant_fact_code = models.CharField(max_length=50, null=True, blank=True)
#    quant_fact_code_data_type = models.CharField(max_length=10, null=True, blank=True)
#    pneg = models.CharField(max_length=255, null=True, blank=True)
#    restrictions = models.CharField(max_length=255, null=True, blank=True)
#    completed = models.DateTimeField(null=True, blank=True)
#    result_event = models.CharField(max_length=36, null=True, blank=True)
#    request_event = models.CharField(max_length=36, null=True, blank=True)
#    dqi = models.CharField(max_length=50, null=True, blank=True)
#    origin = models.CharField(max_length=50, null=True, blank=True)
#    author = models.CharField(max_length=50, null=True, blank=True)
#    assoc_code = models.CharField(max_length=50, null=True, blank=True)
#    assoc_src = models.CharField(max_length=50, null=True, blank=True)
#    assoc_code2 = models.CharField(max_length=50, null=True, blank=True)
#    assoc_src2 = models.CharField(max_length=50, null=True, blank=True)
#    image_link = models.CharField(max_length=255, null=True, blank=True)
#    reference_link = models.CharField(max_length=255, null=True, blank=True)
#    format_code = models.CharField(max_length=50, null=True, blank=True)
#    dtd_id = models.CharField(max_length=36, null=True, blank=True)
#    outline_id = models.CharField(max_length=36, null=True, blank=True)
#    snip_id = models.CharField(max_length=36, null=True, blank=True)
#    md_id = models.CharField(max_length=36, null=True, blank=True)
#    isfinding = models.CharField(max_length=1, null=True, blank=True)
#    stext = models.CharField(max_length=255, null=True, blank=True)
#    outline_seq = models.IntegerField(null=True, blank=True)
#    item_seq = models.IntegerField(null=True, blank=True)
#
#
#class EventDiagrams(BaseModel):
#    eventid = models.CharField(max_length=36)
#    detailid = models.CharField(max_length=36)
#    bits = models.TextField()
#    sort = models.IntegerField()
#    caption = models.TextField()
#    episodeid = models.CharField(max_length=36, null=True, blank=True)
#    episodeseq = models.IntegerField(null=True, blank=True)
#
#
#class FdaProduct(BaseModel):
#    applno = models.CharField(max_length=50)
#    productno = models.CharField(max_length=50)
#    form = models.CharField(max_length=255, null=True, blank=True)
#    dosage = models.CharField(max_length=255, null=True, blank=True)
#    productmktstatus = models.IntegerField(null=True, blank=True)
#    tecode = models.CharField(max_length=50, null=True, blank=True)
#    referencedrug = models.CharField(max_length=1, null=True, blank=True)
#    drugname = models.CharField(max_length=255, null=True, blank=True)
#    activeingred = models.CharField(max_length=255, null=True, blank=True)
#
#
#class GlobalParam(BaseModel):
#    p_name = models.CharField(max_length=50)
#    p_value = models.CharField(max_length=50, null=True, blank=True)
#    p_data_type = models.CharField(max_length=50, null=True, blank=True)
#    p_units = models.CharField(max_length=50, null=True, blank=True)
#    p_date_value = models.DateTimeField(null=True, blank=True)
#    p_fact_code = models.CharField(max_length=50, null=True, blank=True)
#    p_fact_code_data_type = models.CharField(max_length=10, null=True, blank=True)
#    p_quant_fact_code = models.CharField(max_length=50, null=True, blank=True)
#    p_quant_fact_code_data_type = models.CharField(max_length=10, null=True, blank=True)
#    p_coded_value = models.CharField(max_length=20, null=True, blank=True)
#    p_code_src = models.CharField(max_length=10, null=True, blank=True)
#    p_semantic_code = models.CharField(max_length=50, null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    acl = models.CharField(max_length=255, null=True, blank=True)
#    p_return = models.CharField(max_length=1, null=True, blank=True)
#
#
#class LabDetail(BaseModel):
#    lab_id = models.CharField(max_length=36)
#    detail_id = models.CharField(max_length=36)
#    pid = models.CharField(max_length=36, null=True, blank=True)
#    specimen_source = models.CharField(max_length=20, null=True, blank=True)
#    result_group = models.CharField(max_length=100, null=True, blank=True)
#    result_category = models.CharField(max_length=100, null=True, blank=True)
#    result_name = models.CharField(max_length=50, null=True, blank=True)
#    result = models.CharField(max_length=255, null=True, blank=True)
#    result_data_type = models.CharField(max_length=10, null=True, blank=True)
#    remark = models.TextField(null=True, blank=True)
#    quant_value = models.CharField(max_length=255, null=True, blank=True)
#    quant_value_data_type = models.CharField(max_length=10, null=True, blank=True)
#    units = models.CharField(max_length=50, null=True, blank=True)
#    site = models.CharField(max_length=50, null=True, blank=True)
#    method = models.CharField(max_length=50, null=True, blank=True)
#    frequency = models.CharField(max_length=50, null=True, blank=True)
#    duration = models.CharField(max_length=50, null=True, blank=True)
#    strength = models.CharField(max_length=50, null=True, blank=True)
#    lot_num = models.CharField(max_length=20, null=True, blank=True)
#    coded_value = models.CharField(max_length=20, null=True, blank=True)
#    code_src = models.CharField(max_length=10, null=True, blank=True)
#    abnormal_flag = models.CharField(max_length=10, null=True, blank=True)
#    status_code = models.CharField(max_length=10, null=True, blank=True)
#    prob_link = models.CharField(max_length=255, null=True, blank=True)
#    dx_flag = models.IntegerField(null=True, blank=True)
#    action_taken = models.CharField(max_length=50, null=True, blank=True)
#    quant_date = models.DateTimeField(null=True, blank=True)
#    sem_code = models.CharField(max_length=50, null=True, blank=True)
#    fact_code = models.CharField(max_length=50, null=True, blank=True)
#    fact_code_data_type = models.CharField(max_length=10, null=True, blank=True)
#    quant_fact_code = models.CharField(max_length=50, null=True, blank=True)
#    quant_fact_code_data_type = models.CharField(max_length=10, null=True, blank=True)
#    pneg = models.CharField(max_length=255, null=True, blank=True)
#    restrictions = models.CharField(max_length=255, null=True, blank=True)
#    completed = models.DateTimeField(null=True, blank=True)
#    result_event = models.CharField(max_length=36, null=True, blank=True)
#    request_event = models.CharField(max_length=36, null=True, blank=True)
#    dqi = models.CharField(max_length=25, null=True, blank=True)
#    origin = models.CharField(max_length=10, null=True, blank=True)
#    author = models.CharField(max_length=50, null=True, blank=True)
#    assoc_code = models.CharField(max_length=50, null=True, blank=True)
#    assoc_src = models.CharField(max_length=50, null=True, blank=True)
#    image_link = models.CharField(max_length=255, null=True, blank=True)
#    reference_link = models.CharField(max_length=255, null=True, blank=True)
#    format_code = models.CharField(max_length=50, null=True, blank=True)
#    assoc_code2 = models.CharField(max_length=50, null=True, blank=True)
#    assoc_src2 = models.CharField(max_length=50, null=True, blank=True)
#    ref_range = models.CharField(max_length=50, null=True, blank=True)
#    probability = models.CharField(max_length=50, null=True, blank=True)
#    nature_abnormal_test = models.CharField(max_length=255, null=True, blank=True)
#    rev_level = models.CharField(max_length=50, null=True, blank=True)
#    observation_date = models.DateTimeField(null=True, blank=True)
#    test_ordered = models.CharField(max_length=255, null=True, blank=True)
#    individual_test_display_name = models.CharField(max_length=255, null=True, blank=True)
#    sort_within_panel = models.IntegerField(null=True, blank=True)
#
#
#class LabMaster(BaseModel):
#    lab_id = models.CharField(max_length=36)
#    event_id = models.CharField(max_length=36, null=True, blank=True)
#    pid = models.CharField(max_length=36, null=True, blank=True)
#    prior_event_link = models.CharField(max_length=36, null=True, blank=True)
#    event_type = models.CharField(max_length=30, null=True, blank=True)
#    record_type = models.CharField(max_length=50, null=True, blank=True)
#    document_title = models.CharField(max_length=75, null=True, blank=True)
#    document_type = models.CharField(max_length=50, null=True, blank=True)
#    rev_level = models.CharField(max_length=20, null=True, blank=True)
#    link_type = models.CharField(max_length=15, null=True, blank=True)
#    order_id = models.CharField(max_length=36, null=True, blank=True)
#    ordered_by = models.CharField(max_length=36, null=True, blank=True)
#    performedby_id = models.CharField(max_length=36, null=True, blank=True)
#    performed_by = models.CharField(max_length=50, null=True, blank=True)
#    request_date = models.DateTimeField(null=True, blank=True)
#    event_date = models.DateTimeField(null=True, blank=True)
#    event_loc = models.CharField(max_length=36, null=True, blank=True)
#    system_date = models.DateTimeField(null=True, blank=True)
#    event = models.TextField(null=True, blank=True)
#    interpretedby_id = models.CharField(max_length=36, null=True, blank=True)
#    interpreted_by = models.CharField(max_length=50, null=True, blank=True)
#    priority = models.CharField(max_length=10, null=True, blank=True)
#    event_completion_status = models.CharField(max_length=10, null=True, blank=True)
#    lab_section_id = models.CharField(max_length=10, null=True, blank=True)
#    action_code = models.CharField(max_length=10, null=True, blank=True)
#    relevant_clin_info = models.CharField(max_length=255, null=True, blank=True)
#    reason = models.CharField(max_length=255, null=True, blank=True)
#    completion_date = models.DateTimeField(null=True, blank=True)
#    org = models.CharField(max_length=50, null=True, blank=True)
#    dept = models.CharField(max_length=50, null=True, blank=True)
#    forward_link = models.CharField(max_length=36, null=True, blank=True)
#    age_at_event = models.IntegerField(null=True, blank=True)
#    restrictions = models.CharField(max_length=255, null=True, blank=True)
#    utc_bias = models.IntegerField(null=True, blank=True)
#    transcribed_by = models.CharField(max_length=36, null=True, blank=True)
#    transcribed_time = models.DateTimeField(null=True, blank=True)
#    transcribed_org = models.CharField(max_length=36, null=True, blank=True)
#    hash = models.CharField(max_length=50, null=True, blank=True)
#    episodeid = models.CharField(max_length=36, null=True, blank=True)
#    renderer = models.CharField(max_length=255, null=True, blank=True)
#    interpreted_date = models.DateTimeField(null=True, blank=True)
#    reviewed_by = models.CharField(max_length=50, null=True, blank=True)
#    reviewedby_id = models.CharField(max_length=36, null=True, blank=True)
#    reviewed_date = models.DateTimeField(null=True, blank=True)
#    evdocloc = models.CharField(max_length=36, null=True, blank=True)
#    episodeseq = models.IntegerField(null=True, blank=True)
#    placer_order_number = models.CharField(max_length=255, null=True, blank=True)
#    filler_order_number = models.CharField(max_length=255, null=True, blank=True)
#
#
#class Lexicon(BaseModel):
#    code_val = models.CharField(max_length=50)
#    src = models.CharField(max_length=50)
#    full_text = models.TextField(null=True, blank=True)
#    sht_text = models.CharField(max_length=255, null=True, blank=True)
#    changeindicator = models.CharField(max_length=1, null=True, blank=True)
#    codestatus = models.CharField(max_length=1, null=True, blank=True)
#
#
#class LexIndex(BaseModel):
#    code_val = models.CharField(max_length=50)
#    word = models.CharField(max_length=50)
#    src = models.CharField(max_length=50)
#
#
#class MainDict(BaseModel):
#    md_id = models.CharField(max_length=36)
#    code_group = models.CharField(max_length=50, null=True, blank=True)
#    fact_code = models.CharField(max_length=50, null=True, blank=True)
#    code_class = models.CharField(max_length=50, null=True, blank=True)
#    norm_flag = models.CharField(max_length=5, null=True, blank=True)
#    stext = models.CharField(max_length=255, null=True, blank=True)
#    sort_seq = models.IntegerField(null=True, blank=True)
#    modifiers = models.CharField(max_length=255, null=True, blank=True)
#    filter_spec = models.CharField(max_length=50, null=True, blank=True)
#    num_copies = models.IntegerField(null=True, blank=True)
#    source = models.CharField(max_length=10, null=True, blank=True)
#    std_code = models.CharField(max_length=50, null=True, blank=True)
#    units = models.CharField(max_length=50, null=True, blank=True)
#    site = models.CharField(max_length=50, null=True, blank=True)
#    method = models.CharField(max_length=50, null=True, blank=True)
#    text_as_section = models.CharField(max_length=1, null=True, blank=True)
#    frequency = models.CharField(max_length=50, null=True, blank=True)
#    duration = models.CharField(max_length=50, null=True, blank=True)
#    strength = models.CharField(max_length=50, null=True, blank=True)
#    lot_num = models.CharField(max_length=1, null=True, blank=True)
#    dunits = models.CharField(max_length=50, null=True, blank=True)
#    dsite = models.CharField(max_length=50, null=True, blank=True)
#    dmethod = models.CharField(max_length=50, null=True, blank=True)
#    dfreq = models.CharField(max_length=50, null=True, blank=True)
#    ddur = models.CharField(max_length=50, null=True, blank=True)
#    dstren = models.CharField(max_length=50, null=True, blank=True)
#    destination = models.CharField(max_length=50, null=True, blank=True)
#    num_values = models.IntegerField(null=True, blank=True)
#    dval1 = models.CharField(max_length=50, null=True, blank=True)
#    qmod = models.CharField(max_length=50, null=True, blank=True)
#    qmod_promote = models.CharField(max_length=1, null=True, blank=True)
#    create_date = models.DateTimeField(null=True, blank=True)
#    foundation = models.CharField(max_length=1, null=True, blank=True)
#    auto_prob = models.CharField(max_length=1, null=True, blank=True)
#    auto_dx = models.CharField(max_length=1, null=True, blank=True)
#    inc_dx_mods = models.CharField(max_length=1, null=True, blank=True)
#    auto_track = models.CharField(max_length=1, null=True, blank=True)
#    d_trackact = models.CharField(max_length=50, null=True, blank=True)
#    d_tracktime = models.IntegerField(null=True, blank=True)
#    d_trackunit = models.CharField(max_length=10, null=True, blank=True)
#    auto_curmed = models.CharField(max_length=1, null=True, blank=True)
#    sem_code = models.CharField(max_length=50, null=True, blank=True)
#    udt = models.CharField(max_length=50, null=True, blank=True)
#    cwrule = models.CharField(max_length=50, null=True, blank=True)
#    pneg = models.CharField(max_length=128, null=True, blank=True)
#    full_text = models.TextField(null=True, blank=True)
#    reqd = models.CharField(max_length=1, null=True, blank=True)
#    restrictions = models.CharField(max_length=128, null=True, blank=True)
#    result_data_type = models.CharField(max_length=10, null=True, blank=True)
#    quant_data_type = models.CharField(max_length=10, null=True, blank=True)
#    fact_code_data_type = models.CharField(max_length=10, null=True, blank=True)
#    auto_param = models.CharField(max_length=1, null=True, blank=True)
#    xml_export_tag_name = models.CharField(max_length=50, null=True, blank=True)
#    format_code = models.CharField(max_length=50, null=True, blank=True)
#    edit_date = models.DateTimeField(null=True, blank=True)
#    qlaunch_before = models.CharField(max_length=4096, null=True, blank=True)
#    qlaunch_after = models.CharField(max_length=255, null=True, blank=True)
#    launch_group = models.CharField(max_length=255, null=True, blank=True)
#    auto_dictate = models.CharField(max_length=1, null=True, blank=True)
#    custom_exit = models.CharField(max_length=255, null=True, blank=True)
#    fact_value = models.CharField(max_length=255, null=True, blank=True)
#    isproc = models.CharField(max_length=1, null=True, blank=True)
#    itembackcolor = models.CharField(max_length=36, null=True, blank=True)
#    itemforecolor = models.CharField(max_length=36, null=True, blank=True)
#
#
#class Outlines(BaseModel):
#    item_id = models.CharField(max_length=36)
#    dtd_id = models.CharField(max_length=36, null=True, blank=True)
#    category = models.CharField(max_length=100, null=True, blank=True)
#    parent = models.CharField(max_length=50, null=True, blank=True)
#    display_txt = models.CharField(max_length=50, null=True, blank=True)
#    sort_seq = models.IntegerField(null=True, blank=True)
#    use_nl = models.CharField(max_length=1, null=True, blank=True)
#    use_nc = models.CharField(max_length=1, null=True, blank=True)
#    term_group = models.CharField(max_length=50, null=True, blank=True)
#    nest_level = models.IntegerField(null=True, blank=True)
#    exgroup = models.CharField(max_length=50, null=True, blank=True)
#    all_on = models.CharField(max_length=1, null=True, blank=True)
#    seq = models.IntegerField(null=True, blank=True)
#    parent_seq = models.IntegerField(null=True, blank=True)
#    comma = models.CharField(max_length=1, null=True, blank=True)
#    create_date = models.DateTimeField(null=True, blank=True)
#    tabular = models.CharField(max_length=1, null=True, blank=True)
#    show_code = models.CharField(max_length=1, null=True, blank=True)
#    opnfmt = models.CharField(max_length=1, null=True, blank=True)
#    parafmt = models.CharField(max_length=50, null=True, blank=True)
#    lindent = models.FloatField(null=True, blank=True)
#    flindent = models.FloatField(null=True, blank=True)
#    rindent = models.FloatField(null=True, blank=True)
#    spacing = models.FloatField(null=True, blank=True)
#    is_dx = models.CharField(max_length=1, null=True, blank=True)
#    dx_accum = models.CharField(max_length=1, null=True, blank=True)
#    inc_in_dx = models.CharField(max_length=50, null=True, blank=True)
#    reqd = models.CharField(max_length=1, null=True, blank=True)
#    hide_this_sect = models.CharField(max_length=1, null=True, blank=True)
#    format_string = models.CharField(max_length=50, null=True, blank=True)
#    xml_export_tag_name = models.CharField(max_length=50, null=True, blank=True)
#    hide_sect_label = models.CharField(max_length=1, null=True, blank=True)
#    omit_period = models.CharField(max_length=1, null=True, blank=True)
#    popupmod = models.CharField(max_length=50, null=True, blank=True)
#    auto_dictate = models.CharField(max_length=1, null=True, blank=True)
#    print_txt = models.CharField(max_length=100, null=True, blank=True)
#
#
#class PrintQueue(BaseModel):
#    event_id = models.CharField(max_length=36)
#    date_queued = models.DateTimeField(null=True, blank=True)
#    top_margin = models.IntegerField(null=True, blank=True)
#    bottom_margin = models.IntegerField(null=True, blank=True)
#    left_margin = models.IntegerField(null=True, blank=True)
#    right_margin = models.IntegerField(null=True, blank=True)
#    cust_hdr = models.TextField(null=True, blank=True)
#    cust_ftr = models.TextField(null=True, blank=True)
#    num_copies = models.IntegerField(null=True, blank=True)
#    episodeid = models.CharField(max_length=36, null=True, blank=True)
#    episodemode = models.CharField(max_length=1, null=True, blank=True)
#    firstline = models.TextField(null=True, blank=True)
#    rtfinheaders = models.IntegerField(null=True, blank=True)
#    footerbold = models.IntegerField(null=True, blank=True)
#    bodyfontsize = models.IntegerField(null=True, blank=True)
#    footerfontsize = models.IntegerField(null=True, blank=True)
#    lineabove = models.IntegerField(null=True, blank=True)
#    linebelow = models.IntegerField(null=True, blank=True)
#    devicename = models.CharField(max_length=255, null=True, blank=True)
#    evdocloc = models.CharField(max_length=36, null=True, blank=True)
#    renderer = models.CharField(max_length=255, null=True, blank=True)
#
#
#class Problems(BaseModel):
#    prob_id = models.CharField(max_length=36)
#    pid = models.CharField(max_length=36, null=True, blank=True)
#    problem = models.CharField(max_length=255, null=True, blank=True)
#    remark = models.TextField(null=True, blank=True)
#    coded_value = models.CharField(max_length=20, null=True, blank=True)
#    code_src = models.CharField(max_length=10, null=True, blank=True)
#    examiner_id = models.CharField(max_length=36, null=True, blank=True)
#    attending_id = models.CharField(max_length=36, null=True, blank=True)
#    active_date = models.DateTimeField(null=True, blank=True)
#    inactive_date = models.DateTimeField(null=True, blank=True)
#    system_date = models.DateTimeField(null=True, blank=True)
#    priority = models.CharField(max_length=10, null=True, blank=True)
#    prob_status = models.CharField(max_length=10, null=True, blank=True)
#    episodeid = models.CharField(max_length=36, null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    prob_num = models.IntegerField(null=True, blank=True)
#
#
#class RefillRules(BaseModel):
#    med_name = models.CharField(max_length=50)
#    auth = models.IntegerField(null=True, blank=True)
#    min_days = models.IntegerField(null=True, blank=True)
#    max_days = models.IntegerField(null=True, blank=True)
#
#
#class ServiceLocations(BaseModel):
#    loc_code = models.CharField(max_length=36)
#    description = models.CharField(max_length=50, null=True, blank=True)
#    facility_name = models.CharField(max_length=50, null=True, blank=True)
#    fac_addr1 = models.CharField(max_length=50, null=True, blank=True)
#    fac_addr2 = models.CharField(max_length=50, null=True, blank=True)
#    fac_city = models.CharField(max_length=50, null=True, blank=True)
#    fac_state = models.CharField(max_length=50, null=True, blank=True)
#    fac_zip = models.CharField(max_length=50, null=True, blank=True)
#    fac_phone = models.CharField(max_length=50, null=True, blank=True)
#    fac_fax = models.CharField(max_length=50, null=True, blank=True)
#
#
#class Snippets(BaseModel):
#    snippet_id = models.CharField(max_length=36)
#    code_class = models.CharField(max_length=50, null=True, blank=True)
#    code_group = models.CharField(max_length=50, null=True, blank=True)
#    definition = models.CharField(max_length=255, null=True, blank=True)
#    smode = models.CharField(max_length=10, null=True, blank=True)
#    num_ret_val = models.IntegerField(null=True, blank=True)
#    corpus = models.TextField(null=True, blank=True)
#    create_date = models.DateTimeField(null=True, blank=True)
#    edit_date = models.DateTimeField(null=True, blank=True)
#    filter_param = models.CharField(max_length=50, null=True, blank=True)
#    filter_test = models.CharField(max_length=5, null=True, blank=True)
#    filter_null = models.CharField(max_length=5, null=True, blank=True)
#
#
#class SnippetClasses(BaseModel):
#    snippet_class = models.CharField(max_length=50)
#    structural = models.CharField(max_length=1, null=True, blank=True)
#
#
#class Sources(BaseModel):
#    source = models.CharField(max_length=50)
#
#
#class SyncControl(BaseModel):
#    table_name = models.CharField(max_length=50)
#    last_export = models.DateTimeField(null=True, blank=True)
#    last_import = models.DateTimeField(null=True, blank=True)
#
#
#class Tracking(BaseModel):
#    track_id = models.CharField(max_length=36)
#    pid = models.CharField(max_length=36, null=True, blank=True)
#    event_id = models.CharField(max_length=36, null=True, blank=True)
#    item_group = models.CharField(max_length=50, null=True, blank=True)
#    item_category = models.CharField(max_length=50, null=True, blank=True)
#    item_name = models.CharField(max_length=50, null=True, blank=True)
#    item_value = models.CharField(max_length=255, null=True, blank=True)
#    item_comment = models.TextField(null=True, blank=True)
#    action_taken = models.CharField(max_length=50, null=True, blank=True)
#    coded_value = models.CharField(max_length=20, null=True, blank=True)
#    code_src = models.CharField(max_length=10, null=True, blank=True)
#    assigned_to = models.CharField(max_length=36, null=True, blank=True)
#    physician_id = models.CharField(max_length=36, null=True, blank=True)
#    init_date = models.DateTimeField(null=True, blank=True)
#    due_date = models.DateTimeField(null=True, blank=True)
#    review_date = models.DateTimeField(null=True, blank=True)
#    review_by = models.CharField(max_length=36, null=True, blank=True)
#    completion_date = models.DateTimeField(null=True, blank=True)
#    completed_by = models.CharField(max_length=36, null=True, blank=True)
#    priority = models.CharField(max_length=25, null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    status_code = models.CharField(max_length=25, null=True, blank=True)
#    episodeid = models.CharField(max_length=36, null=True, blank=True)
#    reference_link = models.CharField(max_length=50, null=True, blank=True)
#
#
#class TranscriptionDetail(BaseModel):
#    event_id = models.CharField(max_length=36)
#    slot_id = models.CharField(max_length=50)
#    item_content = models.TextField(null=True, blank=True)
#    created = models.DateTimeField(null=True, blank=True)
#    created_by = models.CharField(max_length=50, null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    updated_by = models.CharField(max_length=50, null=True, blank=True)
#
#
#class TranscriptionMaster(BaseModel):
#    event_id = models.CharField(max_length=36)
#    pid = models.CharField(max_length=36, null=True, blank=True)
#    performedby_id = models.CharField(max_length=36, null=True, blank=True)
#    interpretedby_id = models.CharField(max_length=36, null=True, blank=True)
#    date_queued = models.DateTimeField(null=True, blank=True)
#    maxitemnumber = models.IntegerField(null=True, blank=True)
#    transcribed_by = models.CharField(max_length=36, null=True, blank=True)
#    transcribed_time = models.DateTimeField(null=True, blank=True)
#    transcribing_org = models.CharField(max_length=36, null=True, blank=True)
#    sending_org = models.CharField(max_length=50, null=True, blank=True)
#    sending_dept = models.CharField(max_length=50, null=True, blank=True)
#    transcription_complete = models.CharField(max_length=1, null=True, blank=True)
#    review_time = models.DateTimeField(null=True, blank=True)
#    review_complete = models.CharField(max_length=1, null=True, blank=True)
#    corrected_time = models.DateTimeField(null=True, blank=True)
#    corrections_complete = models.CharField(max_length=1, null=True, blank=True)
#    approved_as_final_time = models.DateTimeField(null=True, blank=True)
#    approved_as_final = models.CharField(max_length=1, null=True, blank=True)
#    original_body = models.TextField(null=True, blank=True)
#    original_hash = models.CharField(max_length=50, null=True, blank=True)
#    created = models.DateTimeField(null=True, blank=True)
#    created_by = models.CharField(max_length=50, null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    updated_by = models.CharField(max_length=50, null=True, blank=True)
#    episodeid = models.CharField(max_length=36, null=True, blank=True)
#    reviewedby_id = models.CharField(max_length=36, null=True, blank=True)
#    reviewed_date = models.DateTimeField(null=True, blank=True)
#    reviewed_by = models.CharField(max_length=50, null=True, blank=True)
#
#
#class TranscriptionQueue(BaseModel):
#    event_id = models.CharField(max_length=36)
#    pid = models.CharField(max_length=36, null=True, blank=True)
#    episodeid = models.CharField(max_length=36, null=True, blank=True)
#    performedby_id = models.CharField(max_length=36, null=True, blank=True)
#    interpretedby_id = models.CharField(max_length=36, null=True, blank=True)
#    date_queued = models.DateTimeField(null=True, blank=True)
#    maxitemnumber = models.IntegerField(null=True, blank=True)
#    transcribing_org = models.CharField(max_length=36, null=True, blank=True)
#    sending_org = models.CharField(max_length=50, null=True, blank=True)
#    sending_dept = models.CharField(max_length=50, null=True, blank=True)
#    service = models.CharField(max_length=50, null=True, blank=True)
#    team = models.CharField(max_length=50, null=True, blank=True)
#    status = models.IntegerField(null=True, blank=True)
#    transcribed_by = models.CharField(max_length=36, null=True, blank=True)
#    transcribed_time = models.DateTimeField(null=True, blank=True)
#    review_time = models.DateTimeField(null=True, blank=True)
#    review_complete = models.CharField(max_length=1, null=True, blank=True)
#    corrected_time = models.DateTimeField(null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    updated_by = models.CharField(max_length=36, null=True, blank=True)
#    session_id = models.CharField(max_length=36, null=True, blank=True)
#    transcription_complete = models.CharField(max_length=1, null=True, blank=True)
#    time_completed = models.DateTimeField(null=True, blank=True)
#
#
#class TranscriptionStatus(BaseModel):
#    description = models.CharField(max_length=50)
#    status = models.IntegerField(null=True, blank=True)
#
#
#class UserControl(BaseModel):
#    setting_name = models.CharField(max_length=50)
#    setting_type = models.IntegerField(null=True, blank=True)
#    num_setting = models.IntegerField(null=True, blank=True)
#    str_setting = models.CharField(max_length=50, null=True, blank=True)
#    date_setting = models.DateTimeField(null=True, blank=True)
#
#
#class WorkstationSettings(BaseModel):
#    rootkey = models.CharField(max_length=50)
#    sectionname = models.CharField(max_length=50)
#    keyname = models.CharField(max_length=50)
#    setting = models.TextField(null=True, blank=True)
#
#
#class XmmCategories(BaseModel):
#    category = models.CharField(max_length=30)
#
#
#class XmmComments(BaseModel):
#    commentid = models.CharField(max_length=50)
#    linkid = models.CharField(max_length=50)
#    comment = models.TextField()
#    commentby = models.CharField(max_length=30)
#    commentdate = models.DateTimeField()
#
#
#class XmmLink(BaseModel):
#    linkid = models.CharField(max_length=50)
#    mode = models.CharField(max_length=10)
#    extension = models.CharField(max_length=50, null=True, blank=True)
#    embeddedobject = models.TextField(null=True, blank=True)
#    linkedobject = models.TextField(null=True, blank=True)
#    description = models.CharField(max_length=255, null=True, blank=True)
#    category = models.CharField(max_length=30, null=True, blank=True)
#    objectcreateddatetime = models.DateTimeField()
#    objectmodifieddatetime = models.DateTimeField()
#    linkcreateddatetime = models.DateTimeField()
#    linkmodifieddatetime = models.DateTimeField()
#    auxiliarydatetime = models.DateTimeField(null=True, blank=True)
#    createdby = models.CharField(max_length=20)
#    modifiedby = models.CharField(max_length=20)
#    entityid = models.CharField(max_length=50)
#    eventdatetime = models.DateTimeField(null=True, blank=True)
#    eventid = models.CharField(max_length=50, null=True, blank=True)
#    episodeid = models.CharField(max_length=50, null=True, blank=True)
#    resultgroup = models.CharField(max_length=100, null=True, blank=True)
#    resultcategory = models.CharField(max_length=100, null=True, blank=True)
#    location = models.CharField(max_length=10, null=True, blank=True)
#    linkisbroken = models.CharField(max_length=1)
#    linkbrokendatetime = models.DateTimeField(null=True, blank=True)
#    linkbrokenby = models.CharField(max_length=20, null=True, blank=True)
#    linkismodel = models.CharField(max_length=1)
