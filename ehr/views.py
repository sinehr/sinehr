#from django.shortcuts import render
#from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
#from rest_framework.decorators import authentication_classes, permission_classes
#from rest_framework.permissions import IsAuthenticated
#from rest_framework.response import Response
#from rest_framework.authtoken.views import ObtainAuthToken
#from rest_framework.authtoken.models import Token
#from django.contrib.auth.models import User
#from django.contrib.auth import authenticate
#from django.contrib.auth import views
#
#
#from rest_framework import viewsets, status
#import models
#import serializers
#
#
#class ApiViewSet(viewsets.ModelViewSet):
#    """
#    Api View Set
#    """
#    permission_classes = (IsAuthenticated, )
#    authentication_classes = (SessionAuthentication, TokenAuthentication, )
#
#
#class EntityViewSet(ApiViewSet):
#    """
#    Entities
#    """
#    queryset = models.Entity.objects.all()
#    serializer_class = serializers.EntitySerializer
#
#
#class TokenView(ObtainAuthToken):
#    def post(self, request):
#
#        user = authenticate(username=request.POST['username'], password=request.POST['password'])
#        if user is not None:
#            token, created = Token.objects.get_or_create(user=user)
#
#            return Response({'token': token.key})
#        return Response("Login failed", status=status.HTTP_401_UNAUTHORIZED)
#
#
#obtain_auth_token = TokenView.as_view()
#
#
#def index(request):
#    return render(request, 'common/index.html')