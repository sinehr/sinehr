#!/bin/sh

# To install salt:
#
# wget https://bitbucket.org/sinehr/sinehr/raw/c912c78738325beab279f431429ff2cb2194ac62/platform/setup_salt.sh | sudo sh
#

add-apt-repository ppa:saltstack/salt
apt-get install python-software-properties
apt-get install software-properties-common
apt-get update

apt-get install salt-master
apt-get install salt-minion
apt-get install salt-syndic

