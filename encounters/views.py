from encounters import models, serializers
from system.views import ApiViewSet


class EncounterTimerViewSet(ApiViewSet):
    """
    EncounterTimer
    """
    queryset = models.EncounterTimer.objects.all()
    serializer_class = serializers.EncounterTimerSerializer


class EncounterAttributesViewSet(ApiViewSet):
    """
    EncounterAttributes
    """
    queryset = models.EncounterAttributes.objects.all()
    serializer_class = serializers.EncounterAttributesSerializer


class EncounterRelationshipViewSet(ApiViewSet):
    """
    EncounterRelationship
    """
    queryset = models.EncounterRelationship.objects.all()
    serializer_class = serializers.EncounterRelationshipSerializer


class EncounterDateViewSet(ApiViewSet):
    """
    EncounterDate
    """
    queryset = models.EncounterDate.objects.all()
    serializer_class = serializers.EncounterDateSerializer


class EncounterEntityViewSet(ApiViewSet):
    """
    EncounterEntity
    """
    queryset = models.EncounterEntity.objects.all()
    serializer_class = serializers.EncounterEntitySerializer


class EncounterUnderlaymentViewSet(ApiViewSet):
    """
    EncounterUnderlayment
    """
    queryset = models.EncounterUnderlayment.objects.all()
    serializer_class = serializers.EncounterUnderlaymentSerializer


class EncounterViewSet(ApiViewSet):
    """
    Encounter
    """
    queryset = models.Encounter.objects.all()
    serializer_class = serializers.EncounterSerializer


class EpisodeEntityViewSet(ApiViewSet):
    """
    EpisodeEntity
    """
    queryset = models.EpisodeEntity.objects.all()
    serializer_class = serializers.EpisodeEntitySerializer


class EpisodeAttributesViewSet(ApiViewSet):
    """
    EpisodeAttributes
    """
    queryset = models.EpisodeAttributes.objects.all()
    serializer_class = serializers.EpisodeAttributesSerializer


class EpisodeDateViewSet(ApiViewSet):
    """
    EpisodeDate
    """
    queryset = models.EpisodeDate.objects.all()
    serializer_class = serializers.EpisodeDateSerializer


class EpisodeViewSet(ApiViewSet):
    """
    Episode
    """
    queryset = models.Episode.objects.all()
    serializer_class = serializers.EpisodeSerializer


class EpisodeStageViewSet(ApiViewSet):
    """
    EpisodeStage
    """
    queryset = models.EpisodeStage.objects.all()
    serializer_class = serializers.EpisodeStageSerializer


class DetailAttributesViewSet(ApiViewSet):
    """
    DetailAttributes
    """
    queryset = models.DetailAttributes.objects.all()
    serializer_class = serializers.DetailAttributesSerializer


class DetailMeasurementsViewSet(ApiViewSet):
    """
    DetailMeasurements
    """
    queryset = models.DetailMeasurements.objects.all()
    serializer_class = serializers.DetailMeasurementsSerializer


class DetailQuantitativeMeasurementsViewSet(ApiViewSet):
    """
    DetailQuantitativeMeasurements
    """
    queryset = models.DetailQuantitativeMeasurements.objects.all()
    serializer_class = serializers.DetailQuantitativeMeasurementsSerializer


class DetailQuantitativeValuesViewSet(ApiViewSet):
    """
    DetailQuantitativeValues
    """
    queryset = models.DetailQuantitativeValues.objects.all()
    serializer_class = serializers.DetailQuantitativeValuesSerializer


class DetailCodesViewSet(ApiViewSet):
    """
    DetailCodes
    """
    queryset = models.DetailCodes.objects.all()
    serializer_class = serializers.DetailCodesSerializer


class DetailViewSet(ApiViewSet):
    """
    Detail
    """
    queryset = models.Detail.objects.all()
    serializer_class = serializers.DetailSerializer


