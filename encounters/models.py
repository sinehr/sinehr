from django.db import models
from crux.models import BaseModel, Underlayment, DataTypes, Outline, OutlineItem, List, ListItem
from entities.models import Entity
from system.models import ExtendedUserInformation, Location


class EncounterTimer(BaseModel):
    encounter = models.ForeignKey("Encounter", related_name="timers")
    name = models.CharField(max_length=50, null=True, blank=True)
    models.IntegerField(null=True, blank=True)


class EncounterAttributes(BaseModel):
    encounter = models.ForeignKey("Encounter", related_name="attributes")
    name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    value = models.CharField(max_length=255, null=True, blank=True, db_index=True)


class EncounterRelationship(BaseModel):
    encounter = models.ForeignKey("Encounter", related_name="users")
    user = models.ForeignKey(ExtendedUserInformation, null=True, blank=True, related_name="+")
    type = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    value = models.CharField(max_length=255, null=True, blank=True, db_index=True)
    date = models.DateTimeField(null=True, blank=True)


class EncounterDate(BaseModel):
    encounter = models.ForeignKey("Encounter", related_name="dates")
    type = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    date = models.DateTimeField(null=True, blank=True)


class EncounterEntity(BaseModel):
    encounter = models.ForeignKey("Encounter", related_name="entities")
    entity = models.ForeignKey(Entity, related_name="encounters")
    type = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    age = models.IntegerField(null=True, blank=True)


class EncounterUnderlayment(BaseModel):
    encounter = models.ForeignKey("Encounter", related_name="+")
    underlayment = models.ForeignKey(Underlayment, related_name="+")
    type = models.CharField(max_length=50, null=True, blank=True, db_index=True)


class Encounter(BaseModel):
#EventMaster
    """
    Encounter records
    """
    #event_id >> export_id
    #pid, age_at_event handled by EncounterEntity record(s)
    #prior_event_link
    prior_encounter = models.ForeignKey("Encounter", related_name="next_encounter", null=True, blank=True)
    #event_type
    type = models.CharField(max_length=50, null=True, blank=True)
    #record_type handled by EncounterAttribute named "record_type"
    #document_title handled by EncounterAttribute named "document_title"
    #document_type handled by EncounterAttribute named "document_type"
    #rev_level handled by EncounterAttribute named "revision_level"
    #link_type handled by EncounterAttribute named "link_type"
    #order_id handled by EncounterAttribute named "order_id"

    ordered_by = models.ForeignKey(ExtendedUserInformation, null=True, blank=True, related_name="+")
    #performedby_id,performed_by handled by EncounterRelationship record type of "performer"
    #interpretedby_id, interpreted_by, interpreted_date handled by EncounterRelationship record type of "interpreter"
    #transcribed_by, transcribed_time, transcribed_org handled by EncounterRelationship record type of "transcriber"
    #reviewedby_id, reviewed_by, reviewed_date handled by EncounterRelationship record type of "reviewer"

    #request_date handled by Date record type of "request"
    #event_date handled by Date record type of "encounter"
    #system_date handled by Date record type of "system"
    #completion_date handled by Date record type of "completion"

    #event_loc
    location = models.ForeignKey(Location, related_name="+", null=True, blank=True)
    #evdocloc maps to service_location.loc_code then maps out the service location id
    documentation_location = models.ForeignKey(Location, related_name="+", null=True, blank=True)

    #event
    corpus = models.TextField(null=True, blank=True)
    priority = models.CharField(max_length=10, null=True, blank=True)
    #event_completion_status
    completion_status = models.CharField(max_length=10, null=True, blank=True)
    #lab_section_id handled by EncounterAttribute named "lab_section"
    action_code = models.CharField(max_length=10, null=True, blank=True)
    #relevant_clin_info handled by EncounterAttribute named "clinical_info"
    #reason handled by EncounterAttribute named "reason"
    #org handled by EncounterAttribute named "organization"
    #dept handled by EncounterAttribute named "department"
    #forward_link handled by prior link relationship
    #restrictions handled by EncounterAttribute named "restrictions"
    #utc_bias handled by EncounterAttribute named "utc_bias"
    #hash handled by EncounterAttribute named "hash"
    #episodeid
    episode = models.ForeignKey("Episode", related_name="encounters", null=True, blank=True)
    #episodeseq
    episode_sequence_index = models.IntegerField(null=True, blank=True)
    renderer = models.CharField(max_length=255, null=True, blank=True)

    #page1uid handled by EncounterUnderlayment record type of "first_page"
    #pageevenuid handled by EncounterUnderlayment record type of "even_page"
    #pageodduid handled by EncounterUnderlayment record type of "odd_page"
    #encounter_time handled by EncounterTimer named "overall"
    #face_to_face_time handled by EncounterTimer named "interaction"


class EpisodeEntity(BaseModel):
    episodes = models.ForeignKey("Episode", related_name="entities")
    entity = models.ForeignKey(Entity, related_name="episodes")
    type = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    age = models.IntegerField(null=True, blank=True)


class EpisodeAttributes(BaseModel):
    episode = models.ForeignKey("Episode", related_name="attributes")
    name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    value = models.CharField(max_length=255, null=True, blank=True, db_index=True)


class EpisodeDate(BaseModel):
    episode = models.ForeignKey("Episode", related_name="dates")
    type = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    date = models.DateTimeField(null=True, blank=True)


class Episode(BaseModel):
    #episodeid >> export_id
    #pid, ptage handled by EpisodeEntity record(s)
    #facility handled by EpisodeAttribute name of "facility"
    #area handled by EpisodeAttribute name of "area"
    #activitycode handled by EpisodeAttribute name of "activity"
    #to_see handled by EpisodeAttribute name of "to_see" (ExtendedUserInformation id)
    #episodetype
    type = models.CharField(max_length=50, null=True, blank=True)
    #arrivetime
    start = models.DateTimeField(null=True, blank=True)
    #arrivelocation
    start_location = models.CharField(max_length=36, null=True, blank=True)
    #departtime
    end = models.DateTimeField(null=True, blank=True)
    #departlocation
    end_location = models.CharField(max_length=36, null=True, blank=True)
    #episodeclasscode handled by EpisodeAttribute name of "class"
    #chiefcomplaint handled by EpisodeAttribute name of "chief_complaint"
    #camefrom handled by EpisodeAttribute name of "from"
    #sendingfacilityname handled by EpisodeAttribute name of "sending_facility"
    #transmode handled by EpisodeAttribute name of "transcription_mode"
    #ambulanceservice handled by EpisodeAttribute name of "ambulance_service"
    #squad handled by EpisodeAttribute name of "squad"
    #crimevictim handled by EpisodeAttribute name of "crime_victim"
    #policereportrequired handled by EpisodeAttribute name of "police_report_required"
    #policeresponsedate handled by EpisodeDate type of ""police_response_Date"
    #policeofficer handled by EpisodeAttribute name of "police_officer"
    #signinby handled by EpisodeAttribute name of "sign_in_by"
    priority = models.CharField(max_length=50, null=True, blank=True)
    #pttype handled by EpisodeAttribute name of "patient_type"
    #broughtby handled by EpisodeAttribute name of "brought_by"
    #localcontactphone handled by EpisodeAttribute name of "local_contact_phone"
    #disposition handled by EpisodeAttribute name of "disposition"
    #destinationfacilityname handled by EpisodeAttribute name of "destination_facility_name"
    #followupapptreqd handled by EpisodeAttribute name of "followup_appointment_required"
    #signoutby handled by EpisodeAttribute name of "sign_out_by"
    #referredto handled by EpisodeAttribute name of "referred_to"
    #accountnumber handled by EpisodeAttribute name of "account_number"
    #episodecomplete
    complete = models.CharField(max_length=1, null=True, blank=True)
    #reglname handled by EpisodeAttribute name of "registered_last_name"
    #regfname handled by EpisodeAttribute name of "registered_first_name"
    #systemdate handled by EpisodeDate type of ""system"
    #controlnumber handled by EpisodeAttribute name of "control_number"
    #authorizationinfo handled by EpisodeAttribute name of "authorization_information"
    #priorlinkedepisodeid, nextlinkedepisodeid
    next_episode = models.ForeignKey("Episode", related_name="previous_episode", null=True, blank=True)
    #date_changed handled by Revision record


class EpisodeStage(BaseModel):
    #episodeid
    episode = models.ForeignKey("Episode", related_name="stage")
    #currentlocationtime
    arrival = models.DateTimeField(null=True, blank=True)
    #currentlocation
    stage = models.CharField(max_length=50, null=True, blank=True)
    #systemdate
    system = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=50, null=True, blank=True)
    #date_changed handled by Revision record
    #nextstage
    next_stage = models.CharField(max_length=50, null=True, blank=True)


class DetailAttributes(BaseModel):
    detail = models.ForeignKey("Detail", related_name="attributes")
    name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    value = models.CharField(max_length=255, null=True, blank=True, db_index=True)


class DetailMeasurements(BaseModel):
    detail = models.ForeignKey("Detail", related_name="measurements")
    units = models.CharField(max_length=255, null=True, blank=True)
    site = models.CharField(max_length=255, null=True, blank=True)
    method = models.CharField(max_length=255, null=True, blank=True)
    frequency = models.CharField(max_length=255, null=True, blank=True)
    duration = models.CharField(max_length=255, null=True, blank=True)
    strength = models.CharField(max_length=255, null=True, blank=True)


class DetailQuantitativeMeasurements(BaseModel):
    quantitative = models.ForeignKey("DetailQuantitativeValues", related_name="measurements")
    units = models.CharField(max_length=255, null=True, blank=True)
    site = models.CharField(max_length=255, null=True, blank=True)
    method = models.CharField(max_length=255, null=True, blank=True)
    frequency = models.CharField(max_length=255, null=True, blank=True)
    duration = models.CharField(max_length=255, null=True, blank=True)
    strength = models.CharField(max_length=255, null=True, blank=True)


class DetailQuantitativeValues(BaseModel):
    detail = models.ForeignKey("Detail", related_name="quantitatives")
    data_type = models.ForeignKey(DataTypes, related_name="+")
    value = models.CharField(max_length=255, null=True, blank=True)
    date = models.DateTimeField(null=True, blank=True)
    fact_code = models.CharField(max_length=50, null=True, blank=True)
    fact_code_data_type = models.CharField(max_length=10, null=True, blank=True)


class DetailCodes(BaseModel):
    detail = models.ForeignKey("Detail", related_name="codes")
    name = models.CharField(max_length=50, null=True, blank=True)
    value = models.CharField(max_length=50, null=True, blank=True)
    source = models.CharField(max_length=50, null=True, blank=True)
    type = models.ForeignKey("crux.DataTypes", null=True, blank=True, related_name='+')
    sequential_index = models.IntegerField(null=True, blank=True, default=0)
    corpus = models.CharField(max_length=255, null=True, blank=True)
    mode = models.CharField(max_length=1, null=True, blank=True)
    parent = models.ForeignKey("DetailCodes", related_name="associated_codes", null=True, blank=True)


#class EpisodeDx(BaseModel):
# handled in DetailCodes records
#    detail_id
#    event_id
#    episodeid
#    pid
#    dxrank = models.IntegerField(null=True, blank=True)
#    dxdesc = models.CharField(max_length=255, null=True, blank=True)
#    dxcode = models.CharField(max_length=20, null=True, blank=True)
#    dxsrc = models.CharField(max_length=10, null=True, blank=True)
#    has_detail = models.CharField(max_length=1, null=True, blank=True)
#    date_changed = models.DateTimeField(null=True, blank=True)
#    create_date = models.DateTimeField(null=True, blank=True)
#
#
#class EpisodePr(BaseModel):
# handled in DetailCodes records
#    detail_id = models.CharField(max_length=36)
#    event_id = models.CharField(max_length=36, null=True, blank=True)
#    episodeid = models.CharField(max_length=36, null=True, blank=True)
#    episodeseq = models.IntegerField(null=True, blank=True)
#    prcode = models.CharField(max_length=20, null=True, blank=True)
#    prsrc = models.CharField(max_length=10, null=True, blank=True)

# Associated codes records:
#    dxcode_1 = models.CharField(max_length=20, null=True, blank=True)
#    dxsrc_1 = models.CharField(max_length=10, null=True, blank=True)
#    dxcode_2 = models.CharField(max_length=20, null=True, blank=True)
#    dxsrc_2 = models.CharField(max_length=10, null=True, blank=True)
#    dxcode_3 = models.CharField(max_length=20, null=True, blank=True)
#    dxsrc_3 = models.CharField(max_length=10, null=True, blank=True)
#    dxcode_4 = models.CharField(max_length=20, null=True, blank=True)
#    dxsrc_4 = models.CharField(max_length=10, null=True, blank=True)

#    isproc = models.CharField(max_length=1, null=True, blank=True)
#    procmodifiers = models.CharField(max_length=50, null=True, blank=True)


class Detail(BaseModel):
    encounter = models.ForeignKey("Episode", related_name="details")
    #detail_id > export_id
    #pid handled by Encounter/EncounterEntity records
    #specimen_source handled by DetailAttribute named ""
    #result_group
    group = models.CharField(max_length=100, null=True, blank=True)
    #result_category
    category = models.CharField(max_length=100, null=True, blank=True)
    #result_name
    name = models.CharField(max_length=50, null=True, blank=True)
    #result
    value = models.CharField(max_length=255, null=True, blank=True)
    #result_data_type
    data_type = models.ForeignKey(DataTypes, related_name="+")
    #remark
    extended_value = models.TextField(null=True, blank=True)
    #quant_value, quant_value_data_type, quant_date, quant_fact_code, quant_fact_code_data_type
    #   handled by quantitative records
    #units,site,method,frequency,duration,strength handled by measurements records
    #lot_num handled by DetailAttributes record named "lot_number"
    #coded_value,code_src handled by DetailCodes named "standard"
    #abnormal_flag handled by DetailAttributes record named "abnormal_flag"
    #status_code handled by DetailAttributes record named "status"
    #prob_link handled by DetailAttributes record named "problem"
    #dx_flag handled by DetailAttributes record named "is_diagnosis"
    #action_taken handled by DetailAttributes record named "action_taken"
    #sem_code handled by DetailCodes named "semantic"
    #fact_code, fact_code_data_type handled by DetailCodes named "fact"
    #pneg
    negative = models.CharField(max_length=255, null=True, blank=True)
    restrictions = models.CharField(max_length=255, null=True, blank=True)
    completed = models.DateTimeField(null=True, blank=True)
    #result_event
    result_encounter = models.ForeignKey("Encounter", null=True, blank=True, related_name="+")
    #request_event
    request_encounter = models.ForeignKey("Encounter", null=True, blank=True, related_name="+")
    #dqi handled by DetailAttributes record named "dqi"
    #origin handled by DetailAttributes record named "origin"
    author = models.ForeignKey(ExtendedUserInformation, null=True, blank=True, related_name="+")
    #assoc_code, assoc_src handled by DetailCodes named "associated" with sequential_index = 0
    #assoc_code2, assoc_src2 handled by DetailCodes named "associated" with sequential_index = 1
    #image_link handled by DetailAttributes record named "image_link"
    #reference_link handled by DetailAttributes record named "is_finding"
    #format_code handled by DetailCodes named "format"
    #dtd_id
    outline = models.ForeignKey(Outline, null=True, blank=True, related_name="+")
    #outline_id
    outline_item = models.ForeignKey(OutlineItem, null=True, blank=True, related_name="+")
    #snip_id
    list = models.ForeignKey(List, null=True, blank=True, related_name="+")
    #md_id
    list_item = models.ForeignKey(ListItem, null=True, blank=True, related_name="+")
    #isfinding handled by DetailAttributes record named "is_finding"
    #stext handled by DetailAttributes record named "list_item_text"
    #outline_seq handled by DetailAttributes record named "outline_item_sequence_index"
    #item_seq handled by DetailAttributes record named "list_item_sequence"
