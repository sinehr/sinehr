# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crux', '0002_auto_20150703_1801'),
    ]

    operations = [
        migrations.CreateModel(
            name='Detail',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('group', models.CharField(max_length=100, null=True, blank=True)),
                ('category', models.CharField(max_length=100, null=True, blank=True)),
                ('name', models.CharField(max_length=50, null=True, blank=True)),
                ('value', models.CharField(max_length=255, null=True, blank=True)),
                ('extended_value', models.TextField(null=True, blank=True)),
                ('negative', models.CharField(max_length=255, null=True, blank=True)),
                ('restrictions', models.CharField(max_length=255, null=True, blank=True)),
                ('completed', models.DateTimeField(null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='DetailAttributes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('value', models.CharField(db_index=True, max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='DetailCodes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(max_length=50, null=True, blank=True)),
                ('value', models.CharField(max_length=50, null=True, blank=True)),
                ('source', models.CharField(max_length=50, null=True, blank=True)),
                ('sequential_index', models.IntegerField(default=0, null=True, blank=True)),
                ('corpus', models.CharField(max_length=255, null=True, blank=True)),
                ('mode', models.CharField(max_length=1, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='DetailMeasurements',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('units', models.CharField(max_length=255, null=True, blank=True)),
                ('site', models.CharField(max_length=255, null=True, blank=True)),
                ('method', models.CharField(max_length=255, null=True, blank=True)),
                ('frequency', models.CharField(max_length=255, null=True, blank=True)),
                ('duration', models.CharField(max_length=255, null=True, blank=True)),
                ('strength', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='DetailQuantitativeMeasurements',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('units', models.CharField(max_length=255, null=True, blank=True)),
                ('site', models.CharField(max_length=255, null=True, blank=True)),
                ('method', models.CharField(max_length=255, null=True, blank=True)),
                ('frequency', models.CharField(max_length=255, null=True, blank=True)),
                ('duration', models.CharField(max_length=255, null=True, blank=True)),
                ('strength', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='DetailQuantitativeValues',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('value', models.CharField(max_length=255, null=True, blank=True)),
                ('date', models.DateTimeField(null=True, blank=True)),
                ('fact_code', models.CharField(max_length=50, null=True, blank=True)),
                ('fact_code_data_type', models.CharField(max_length=10, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Encounter',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(max_length=50, null=True, blank=True)),
                ('corpus', models.TextField(null=True, blank=True)),
                ('priority', models.CharField(max_length=10, null=True, blank=True)),
                ('completion_status', models.CharField(max_length=10, null=True, blank=True)),
                ('action_code', models.CharField(max_length=10, null=True, blank=True)),
                ('episode_sequence_index', models.IntegerField(null=True, blank=True)),
                ('renderer', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EncounterAttributes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('value', models.CharField(db_index=True, max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EncounterDate',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('date', models.DateTimeField(null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EncounterEntity',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('age', models.IntegerField(null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EncounterRelationship',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('value', models.CharField(db_index=True, max_length=255, null=True, blank=True)),
                ('date', models.DateTimeField(null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EncounterTimer',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(max_length=50, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EncounterUnderlayment',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Episode',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(max_length=50, null=True, blank=True)),
                ('start', models.DateTimeField(null=True, blank=True)),
                ('start_location', models.CharField(max_length=36, null=True, blank=True)),
                ('end', models.DateTimeField(null=True, blank=True)),
                ('end_location', models.CharField(max_length=36, null=True, blank=True)),
                ('priority', models.CharField(max_length=50, null=True, blank=True)),
                ('complete', models.CharField(max_length=1, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EpisodeAttributes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('value', models.CharField(db_index=True, max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EpisodeDate',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('date', models.DateTimeField(null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EpisodeEntity',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('age', models.IntegerField(null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EpisodeStage',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('arrival', models.DateTimeField(null=True, blank=True)),
                ('stage', models.CharField(max_length=50, null=True, blank=True)),
                ('system', models.DateTimeField(null=True, blank=True)),
                ('status', models.CharField(max_length=50, null=True, blank=True)),
                ('next_stage', models.CharField(max_length=50, null=True, blank=True)),
                ('episode', models.ForeignKey(related_name='stage', to='encounters.Episode')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
    ]
