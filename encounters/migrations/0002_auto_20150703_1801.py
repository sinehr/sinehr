# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('encounters', '0001_initial'),
        ('system', '0001_initial'),
        ('entities', '0001_initial'),
        ('crux', '0002_auto_20150703_1801'),
    ]

    operations = [
        migrations.AddField(
            model_name='episodeentity',
            name='entity',
            field=models.ForeignKey(related_name='episodes', to='entities.Entity'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='episodeentity',
            name='episodes',
            field=models.ForeignKey(related_name='entities', to='encounters.Episode'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='episodedate',
            name='episode',
            field=models.ForeignKey(related_name='dates', to='encounters.Episode'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='episodeattributes',
            name='episode',
            field=models.ForeignKey(related_name='attributes', to='encounters.Episode'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='episode',
            name='next_episode',
            field=models.ForeignKey(related_name='previous_episode', blank=True, to='encounters.Episode', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encounterunderlayment',
            name='encounter',
            field=models.ForeignKey(related_name='+', to='encounters.Encounter'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encounterunderlayment',
            name='underlayment',
            field=models.ForeignKey(related_name='+', to='crux.Underlayment'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encountertimer',
            name='encounter',
            field=models.ForeignKey(related_name='timers', to='encounters.Encounter'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encounterrelationship',
            name='encounter',
            field=models.ForeignKey(related_name='users', to='encounters.Encounter'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encounterrelationship',
            name='user',
            field=models.ForeignKey(related_name='+', blank=True, to='system.ExtendedUserInformation', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encounterentity',
            name='encounter',
            field=models.ForeignKey(related_name='entities', to='encounters.Encounter'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encounterentity',
            name='entity',
            field=models.ForeignKey(related_name='encounters', to='entities.Entity'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encounterdate',
            name='encounter',
            field=models.ForeignKey(related_name='dates', to='encounters.Encounter'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encounterattributes',
            name='encounter',
            field=models.ForeignKey(related_name='attributes', to='encounters.Encounter'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encounter',
            name='documentation_location',
            field=models.ForeignKey(related_name='+', blank=True, to='system.Location', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encounter',
            name='episode',
            field=models.ForeignKey(related_name='encounters', blank=True, to='encounters.Episode', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encounter',
            name='location',
            field=models.ForeignKey(related_name='+', blank=True, to='system.Location', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encounter',
            name='ordered_by',
            field=models.ForeignKey(related_name='+', blank=True, to='system.ExtendedUserInformation', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='encounter',
            name='prior_encounter',
            field=models.ForeignKey(related_name='next_encounter', blank=True, to='encounters.Encounter', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detailquantitativevalues',
            name='data_type',
            field=models.ForeignKey(related_name='+', to='crux.DataTypes'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detailquantitativevalues',
            name='detail',
            field=models.ForeignKey(related_name='quantitatives', to='encounters.Detail'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detailquantitativemeasurements',
            name='quantitative',
            field=models.ForeignKey(related_name='measurements', to='encounters.DetailQuantitativeValues'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detailmeasurements',
            name='detail',
            field=models.ForeignKey(related_name='measurements', to='encounters.Detail'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detailcodes',
            name='detail',
            field=models.ForeignKey(related_name='codes', to='encounters.Detail'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detailcodes',
            name='parent',
            field=models.ForeignKey(related_name='associated_codes', blank=True, to='encounters.DetailCodes', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detailcodes',
            name='type',
            field=models.ForeignKey(related_name='+', blank=True, to='crux.DataTypes', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detailattributes',
            name='detail',
            field=models.ForeignKey(related_name='attributes', to='encounters.Detail'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detail',
            name='author',
            field=models.ForeignKey(related_name='+', blank=True, to='system.ExtendedUserInformation', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detail',
            name='data_type',
            field=models.ForeignKey(related_name='+', to='crux.DataTypes'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detail',
            name='encounter',
            field=models.ForeignKey(related_name='details', to='encounters.Episode'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detail',
            name='list',
            field=models.ForeignKey(related_name='+', blank=True, to='crux.List', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detail',
            name='list_item',
            field=models.ForeignKey(related_name='+', blank=True, to='crux.ListItem', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detail',
            name='outline',
            field=models.ForeignKey(related_name='+', blank=True, to='crux.Outline', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detail',
            name='outline_item',
            field=models.ForeignKey(related_name='+', blank=True, to='crux.OutlineItem', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detail',
            name='request_encounter',
            field=models.ForeignKey(related_name='+', blank=True, to='encounters.Encounter', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='detail',
            name='result_encounter',
            field=models.ForeignKey(related_name='+', blank=True, to='encounters.Encounter', null=True),
            preserve_default=True,
        ),
    ]
