from rest_framework import serializers
from encounters import models


class EncounterTimerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.EncounterTimer


class EncounterAttributesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.EncounterAttributes


class EncounterRelationshipSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.EncounterRelationship


class EncounterDateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.EncounterDate


class EncounterEntitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.EncounterEntity


class EncounterUnderlaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.EncounterUnderlayment


class EncounterSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Encounter


class EpisodeEntitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.EpisodeEntity


class EpisodeAttributesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.EpisodeAttributes


class EpisodeDateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.EpisodeDate


class EpisodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Episode


class EpisodeStageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.EpisodeStage


class DetailAttributesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DetailAttributes


class DetailMeasurementsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DetailMeasurements


class DetailQuantitativeMeasurementsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DetailQuantitativeMeasurements


class DetailQuantitativeValuesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DetailQuantitativeValues


class DetailCodesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DetailCodes


class DetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Detail


