from crux import models, serializers
from system.views import ApiViewSet


class BaseModelViewSet(ApiViewSet):
    """
    BaseModel
    """
    queryset = models.BaseModel.objects.all()
    serializer_class = serializers.BaseModelSerializer


class DataTypesViewSet(ApiViewSet):
    """
    DataTypes
    """
    queryset = models.DataTypes.objects.all()
    serializer_class = serializers.DataTypesSerializer


class ListFilterViewSet(ApiViewSet):
    """
    ListFilter
    """
    queryset = models.ListFilter.objects.all()
    serializer_class = serializers.ListFilterSerializer


class ListViewSet(ApiViewSet):
    """
    List
    """
    queryset = models.List.objects.all()
    serializer_class = serializers.ListSerializer


class ListOrStringViewSet(ApiViewSet):
    """
    ListOrString
    """
    queryset = models.ListOrString.objects.all()
    serializer_class = serializers.ListOrStringSerializer


class MeasurementsViewSet(ApiViewSet):
    """
    Measurements
    """
    queryset = models.Measurements.objects.all()
    serializer_class = serializers.MeasurementsSerializer


class ListItemCodesViewSet(ApiViewSet):
    """
    ListItemCodes
    """
    queryset = models.ListItemCodes.objects.all()
    serializer_class = serializers.ListItemCodesSerializer


class ListItemQuantitativeSettingsViewSet(ApiViewSet):
    """
    ListItemQuantitativeSettings
    """
    queryset = models.ListItemQuantitativeSettings.objects.all()
    serializer_class = serializers.ListItemQuantitativeSettingsSerializer


class ListItemViewSet(ApiViewSet):
    """
    ListItem
    """
    queryset = models.ListItem.objects.all()
    serializer_class = serializers.ListItemSerializer


class ListItemAttributesViewSet(ApiViewSet):
    """
    ListItemAttributes
    """
    queryset = models.ListItemAttributes.objects.all()
    serializer_class = serializers.ListItemAttributesSerializer


class ListItemTriggersViewSet(ApiViewSet):
    """
    ListItemTriggers
    """
    queryset = models.ListItemTriggers.objects.all()
    serializer_class = serializers.ListItemTriggersSerializer


class UnderlaymentViewSet(ApiViewSet):
    """
    Underlayment
    """
    queryset = models.Underlayment.objects.all()
    serializer_class = serializers.UnderlaymentSerializer


class OutlineUnderlaymentViewSet(ApiViewSet):
    """
    OutlineUnderlayment
    """
    queryset = models.OutlineUnderlayment.objects.all()
    serializer_class = serializers.OutlineUnderlaymentSerializer


class AdjectivesViewSet(ApiViewSet):
    """
    Adjectives
    """
    queryset = models.Adjectives.objects.all()
    serializer_class = serializers.AdjectivesSerializer


class OutlineAttributesViewSet(ApiViewSet):
    """
    OutlineAttributes
    """
    queryset = models.OutlineAttributes.objects.all()
    serializer_class = serializers.OutlineAttributesSerializer


class OutlineViewSet(ApiViewSet):
    """
    Outline
    """
    queryset = models.Outline.objects.all()
    serializer_class = serializers.OutlineSerializer


class OutlineItemAttributesViewSet(ApiViewSet):
    """
    OutlineItemAttributes
    """
    queryset = models.OutlineItemAttributes.objects.all()
    serializer_class = serializers.OutlineItemAttributesSerializer


class OutlineItemViewSet(ApiViewSet):
    """
    OutlineItem
    """
    queryset = models.OutlineItem.objects.all()
    serializer_class = serializers.OutlineItemSerializer


