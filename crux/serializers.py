from rest_framework import serializers
from crux import models


class BaseModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BaseModel


class DataTypesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DataTypes


class ListFilterSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ListFilter


class ListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.List


class ListOrStringSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ListOrString


class MeasurementsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Measurements


class ListItemCodesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ListItemCodes


class ListItemQuantitativeSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ListItemQuantitativeSettings


class ListItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ListItem


class ListItemAttributesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ListItemAttributes


class ListItemTriggersSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ListItemTriggers


class UnderlaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Underlayment


class OutlineUnderlaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.OutlineUnderlayment


class AdjectivesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Adjectives


class OutlineAttributesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.OutlineAttributes


class OutlineSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Outline


class OutlineItemAttributesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.OutlineItemAttributes


class OutlineItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.OutlineItem
