import hashlib
import uuid
from django.db import models


def default_export_uuid():
    m = hashlib.md5()
    m.update(str(uuid.uuid1())+str(uuid.uuid4())+str(uuid.uuid5(uuid.uuid1(), str(uuid.uuid4()))))
    return str(uuid.uuid5(uuid.uuid4(), m.hexdigest()))


class BaseModel(models.Model):
    """
    Base model for all other models
    """
    export_id = models.CharField(max_length=64, db_index=True, default=default_export_uuid, unique=True)
    creation_user = models.ForeignKey("system.ExtendedUserInformation", null=True, blank=True, related_name='+')
    creation_date = models.DateTimeField(auto_now_add=True, blank=True)
    revision_user = models.ForeignKey("system.ExtendedUserInformation", null=True, blank=True, related_name='+')
    revision_date = models.DateTimeField(auto_now_add=True, blank=True)


class DataTypes(models.Model):
    name = models.CharField(max_length=20, db_index=True, unique=True)


class ListFilter(BaseModel):
    #param
    parameter = models.CharField(max_length=50, null=True, blank=True)
    test = models.CharField(max_length=5, null=True, blank=True)
    null = models.CharField(max_length=5, null=True, blank=True)


class List(BaseModel):
#AppSnippets/Snippets
    #snippet_id >> export_id
    # type = "system", "application"
    type = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    #code_class
    section = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    #code_group
    name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    definition = models.CharField(max_length=255, null=True, blank=True)
    #AppSnippets.mode/Snippets.mode
    mode = models.CharField(max_length=10, null=True, blank=True)
    num_ret_val = models.IntegerField(null=True, blank=True)
    corpus = models.TextField(null=True, blank=True)
    #create_date handled by creation_user/creation_date fields
    #edit_date handled by revision_user/revision_date fields
    # Snippets.filter_param, .filter_test and .filter_null handled in ListFilter
    filter = models.ForeignKey("ListFilter", null=True, blank=True, related_name='+')


class ListOrString(BaseModel):
    list = models.ForeignKey("List", null=True, blank=True)
    value = models.CharField(max_length=50, null=True, blank=True)


class Measurements(BaseModel):
    item = models.ForeignKey("ListItem", db_index=True, related_name='+')
    units = models.ForeignKey("ListOrString", null=True, blank=True, related_name='+')
    site = models.ForeignKey("ListOrString", null=True, blank=True, related_name='+')
    method = models.ForeignKey("ListOrString", null=True, blank=True, related_name='+')
    frequency = models.ForeignKey("ListOrString", null=True, blank=True, related_name='+')
    duration = models.ForeignKey("ListOrString", null=True, blank=True, related_name='+')
    strength = models.ForeignKey("ListOrString", null=True, blank=True, related_name='+')


class ListItemCodes(BaseModel):
    list_item = models.ForeignKey("ListItem", related_name="codes")
    name = models.CharField(max_length=50, null=True, blank=True)
    value = models.CharField(max_length=50, null=True, blank=True)
    type = models.ForeignKey("DataTypes", null=True, blank=True, related_name='+')


class ListItemQuantitativeSettings(BaseModel):
    #dunits handled in quantitative_measurements record
    #dsite handled in quantitative_measurements record
    #dmethod handled in quantitative_measurements record
    #dfreq handled in quantitative_measurements record
    #ddur handled in quantitative_measurements record
    #dstren handled in quantitative_measurements record
    measurements = models.ForeignKey("Measurements", null=True, blank=True)

    #dval1
    value = models.CharField(max_length=50, null=True, blank=True)
    #qmod
    modifier = models.ForeignKey("List", related_name="dependant_items", null=True, blank=True)
    #quant_data_type
    type = models.ForeignKey("DataTypes", null=True, blank=True, related_name='+')

    #qmod_promote
    promotion = models.CharField(max_length=1, null=True, blank=True)
    #qlaunch_before
    before_launch_action = models.CharField(max_length=50, null=True, blank=True)
    #qlaunch_after
    after_launch_action = models.CharField(max_length=50, null=True, blank=True)


class ListItem(BaseModel):
#AppMainDict/MainDict
    #id/md_id >> export_id

    #snippet
    list = models.ForeignKey("List", related_name="items")

    #code_group defined by snippet record
    #code_class defined by snippet record
    #norm_flag handled in ListItemAttributes record with name of "adjective" fact_code contains the adjective code

    #stext
    body = models.CharField(max_length=255, null=True, blank=True)
    #full_text
    extension = models.TextField(null=True, blank=True)

    #pneg
    negative_value = models.CharField(max_length=128, null=True, blank=True)

    #result_data_type
    type = models.ForeignKey("DataTypes", null=True, blank=True, related_name='+')

    #sort_seq
    index = models.IntegerField(null=True, blank=True)

    #modifiers
    qualitative_modifiers = models.ManyToManyField("List", null=True, blank=True)

    #includeif,filter_spec handled in ListItemAttributes record with name of "inclusion_value"

    #num_copies
    instances = models.FloatField(null=True, blank=True)
    source = models.CharField(max_length=10, null=True, blank=True)

    #fact_value handled in ListItemAttributes record with name of "fact_value"

    #std_code handled by ListItemCodes record with name = 'standard'
    #sem_code handled by ListItemCodes record with name = 'semantic'

    #fact_code+fact_code_data_type handled by ListItemCodes record with name = 'fact'
    #fact_code_data_type

    #format_code handled by ListItemCodes record with name = 'format'

    #units handled in measurements record
    #site handled in measurements record
    #method handled in measurements record
    #frequency handled in measurements record
    #duration handled in measurements record
    #strength handled in measurements record
    measurements = models.ForeignKey("Measurements", null=True, blank=True, related_name='+')

    #text_as_section handled in ListItemAttributes record with name of "section_heading" = 'Y/N'
    #lot_num handled in ListItemAttributes record with name of "lot_number"

    #destination handled in ListItemAttributes record with name of "destination"

    #num_values handled in ListItemAttributes record with name of "show_number_pad" = 'Y/N'

    #dunits handled in quantitative_settings record
    #dsite handled in quantitative_settings record
    #dmethod handled in quantitative_settings record
    #dfreq handled in quantitative_settings record
    #ddur handled in quantitative_settings record
    #dstren handled in quantitative_settings record
    #dval1 handled in quantitative_settings record
    #qmod handled in quantitative_settings record
    #quant_data_type handled in quantitative_settings record
    #qmod_promote handled in quantitative_settings record
    #qlaunch_before handled in quantitative_settings record
    #qlaunch_after handled in quantitative_settings record
    quantitative_settings = models.ForeignKey("ListItemQuantitativeSettings", null=True, blank=True, related_name='+')

    #create_date handled by creation_date
    #foundation handled in ListItemAttributes record with name of "foundation"
    #auto_prob tracked in an associated SystemListItemTrigger
    #auto_dx tracked in an associated SystemListItemTrigger
    #inc_dx_mods tracked in an associated SystemListItemTrigger for auto_dx
    #auto_track tracked in an associated SystemListItemTrigger
    #d_trackact handled in ListItemAttributes record with name of "tracking_action"
    #d_tracktime handled in ListItemAttributes record with name of "tracking_time"
    #d_trackunit handled in ListItemAttributes record with name of "tracking_unit"
    #auto_curmed tracked in an associated SystemListItemTrigger
    #udt dropped handled in ListItemAttributes record with name of "udt"
    #rule/cwrule handled in ListItemAttributes record with name of "rule"
    #reqd
    #is_required handled in ListItemAttributes record with name of "required"
    #restrictions handled in ListItemAttributes record with name of "restrictions"
    #auto_param tracked in an associated SystemListItemTrigger
    #xml_export_tag_name handled in ListItemAttributes record with name of "xml_export_tag"
    #edit_date handled by revision_date
    #launch_group handled in ListItemAttributes record with name of "launch_group"
    #auto_dictate tracked in an associated ListItemTrigger
    #custom_exit handled in ListItemAttributes record with name of "custom_exit"
    #isproc tracked in an associated ListItemTrigger
    #itembackcolor handled in ListItemAttributes record with name of "back_color"
    #itemforecolor handled in ListItemAttributes record with name of "fore_color"


class ListItemAttributes(BaseModel):
    list_item = models.ForeignKey("ListItem", related_name="attributes")
    name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    value = models.ForeignKey("ListOrString", null=True, blank=True, related_name='+')


class ListItemTriggers(BaseModel):
    """
    Triggers for ${ListItem} records
    """
    item = models.ForeignKey("ListItem", related_name="triggers")
    type = models.CharField(max_length=50, default="None", db_index=True)
    include_modifiers = models.CharField(max_length=1, null=True, blank=True)


class Underlayment(BaseModel):
#DocUnderlayment
    #uid > export_id
    name = models.CharField(max_length=50)
    description = models.TextField(null=True, blank=True)
    bits = models.TextField()
    available = models.CharField(max_length=1)


class OutlineUnderlayment(BaseModel):
    name = models.CharField(max_length=50)
    outline = models.ForeignKey("Outline", related_name="underlayments")
    underlayment = models.ForeignKey("Underlayment", related_name="outlines")


class Adjectives(BaseModel):
    outline = models.ForeignKey("Outline", related_name="adjectives")
    key = models.CharField(max_length=50, null=True, blank=True)
    default = models.CharField(max_length=50, null=True, blank=True)
    index = models.IntegerField(null=True, blank=True)


class OutlineAttributes(BaseModel):
    outline = models.ForeignKey("Outline", related_name="attributes")
    name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    value = models.ForeignKey("ListOrString", null=True, blank=True, related_name='+')


class Outline(BaseModel):
#DocTypes
    #set_name
    name = models.CharField(max_length=50, null=True, blank=True)
    #rev_level
    revision_level = models.CharField(max_length=5, null=True, blank=True)
    description = models.CharField(max_length=50, null=True, blank=True)
    record_type = models.CharField(max_length=50, null=True, blank=True)
    #normal_key handled by Adjectives record with key of normal_key and default of "Normal" and index of 0
    #in_use
    available = models.CharField(max_length=1, null=True, blank=True)

    #check2 handled by Adjectives record with:
    #       key = check2
    #       default = "Re-examined and found to be unchanged" check2 == 'X'
    #                 "Deferred" for check2 == 'D'
    #       index = 1

    #create_date handled by creation record
    #dtd_id > export_id
    #require_dx handled with OutlineAttributes record named 'require_diagnosis'
    #auto_expand
    expanded = models.CharField(max_length=1, null=True, blank=True)
    #sort_seq
    index = models.IntegerField(null=True, blank=True)
    #copies
    instances = models.IntegerField(null=True, blank=True)
    #d_reason handled with OutlineAttributes record named 'reason'
    definition = models.CharField(max_length=255, null=True, blank=True)
    #cust_hdr handled with OutlineAttributes record named 'header'
    #cust_ftr handled with OutlineAttributes record named 'footer'
    ucase_abn = models.CharField(max_length=1, null=True, blank=True)
    #reqd
    required = models.CharField(max_length=1, null=True, blank=True)
    #compile_date handled with OutlineAttributes record named 'compile_date'
    #compile_path handled with OutlineAttributes record named 'compile_path'
    restrictions = models.CharField(max_length=128, null=True, blank=True)
    #reqcsig  handled with OutlineAttributes record named 'require_co_signer'
    #line_below handled with OutlineAttributes record named 'line_below'
    #line_above handled with OutlineAttributes record named 'line_above'
    #first_line handled with OutlineAttributes record named 'first_line'
    #use_rtf handled with OutlineAttributes record named 'expanded_formatting'
    #cw handled with OutlineAttributes record named 'application_version'
    #post_launch handled with OutlineAttributes record named 'launch_after_close'
    #custom_exit handled with OutlineAttributes record named 'overloaded_exit_handler'
    #save_renderer handled with OutlineAttributes record named 'save_renderer'
    #retain_formatting handled with OutlineAttributes record named 'retain_formatting'
    #save_headers handled with OutlineAttributes record named 'save_headers'
    #save_params handled with OutlineAttributes record named 'save_footers'
    #episodeseq handled with OutlineAttributes record named 'episode_sequence_index'
    #auto_queue handled with OutlineAttributes record named 'queue_for_printing_on_close'
    #trigger_episode_printing handled with OutlineAttributes record named 'queue_episode_for_printing_on_exit'
    #left_margin handled with OutlineAttributes record named 'margin_left'
    #right_margin handled with OutlineAttributes record named 'margin_right'
    #top_margin handled with OutlineAttributes record named 'margin_top'
    #bottom_margin handled with OutlineAttributes record named 'margin_bottom'
    #footerfontsize handled with OutlineAttributes record named 'font_size_footer'
    #bodyfontsize handled with OutlineAttributes record named 'font_size_body'
    #use_custom_headers handled with OutlineAttributes record named 'use_custom_headers'
    #use_custom_layout handled with OutlineAttributes record named 'use_custom_layout'
    #page1uid handled by OutlineUnderlayment record named 'first_page'
    #pageevenuid handled by OutlineUnderlayment record named 'even_pages'
    #pageodduid handled by OutlineUnderlayment record named 'odd_pages'


class OutlineItemAttributes(BaseModel):
    outline_item = models.ForeignKey("OutlineItem", related_name="attributes")
    name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    value = models.ForeignKey("ListOrString", null=True, blank=True, related_name='+')


class OutlineItem(BaseModel):
#Outlines
    #item_id > export_id
    #dtd_id
    outline = models.ForeignKey("Outline", related_name="items")
    parent = models.ForeignKey("OutlineItem", related_name="children")
    #category
    name = models.CharField(max_length=100, null=True, blank=True)
    #display_txt
    label = models.CharField(max_length=50, null=True, blank=True)
    #print_txt
    value = models.CharField(max_length=100, null=True, blank=True)
    #sort_seq
    index = models.IntegerField(null=True, blank=True)
    #use_nl
    #use_nc
    adjectives = models.ManyToManyField("Adjectives", null=True, blank=True, related_name="+")
    #term_group
    list = models.ForeignKey("List", related_name="+")
    #nest_level handled by parent child relationships
    #exgroup handled with OutlineAttributes record named 'inclusion_field'

    #all_on Null => N
    SelectionModes = (
        ('Y', 'Yielded'),   # Section name included, items included on selection
        ('N', 'Normal'),    # Section name and items not included until selected
        ('A', 'All'),       # Section name and items included regardless of being selected
    )
    selection_mode = models.CharField(max_length=1, null=True, blank=True, choices=SelectionModes)
    #seq/parent_seq handled with new value of sequence index which is position inside of parent
    sequence_index = models.IntegerField(null=True, blank=True)
    #comma handled with OutlineAttributes record named 'list_format'
    #tabular handled with OutlineAttributes record named 'tabular_format'
    #opnfmt handled with OutlineAttributes record named 'open_format'
    #parafmt handled with OutlineAttributes record named 'paragraph_format'

    #create_date handled by Creation model fields
    show_code = models.CharField(max_length=1, null=True, blank=True)

    #lindent handled with OutlineAttributes record named 'left_indent'
    #flindent handled with OutlineAttributes record named 'floating_indent'
    #rindent handled with OutlineAttributes record named 'right_indent'
    #spacing handled with OutlineAttributes record named 'line_spacing'

    #is_dx handled with OutlineAttributes record named 'is_dx'
    #dx_accum handled with OutlineAttributes record named 'dx_accum'
    #inc_in_dx handled with OutlineAttributes record named 'inc_in_dx'

    #reqd
    required = models.CharField(max_length=1, null=True, blank=True)
    #hide_this_sect
    hidden = models.CharField(max_length=1, null=True, blank=True)

    #format_string handled with OutlineAttributes record named 'format_string'
    #xml_export_tag_name handled with OutlineAttributes record named 'xml_export_tag_name'

    #hide_sect_label with OutlineAttributes record named 'hide_sect_label'
    #omit_period with OutlineAttributes record named 'omit_period'

    #popupmod
    section_modifier = models.ForeignKey("List", related_name="+")

    #auto_dictate handled with OutlineAttributes record named 'auto_dictate'