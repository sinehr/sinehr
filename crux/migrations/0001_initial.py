# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import crux.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BaseModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('export_id', models.CharField(default=crux.models.default_export_uuid, max_length=36, db_index=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('revision_date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Adjectives',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('key', models.CharField(max_length=50, null=True, blank=True)),
                ('default', models.CharField(max_length=50, null=True, blank=True)),
                ('index', models.IntegerField(null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='DataTypes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20, db_index=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='List',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('section', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('name', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('definition', models.CharField(max_length=255, null=True, blank=True)),
                ('mode', models.CharField(max_length=10, null=True, blank=True)),
                ('num_ret_val', models.IntegerField(null=True, blank=True)),
                ('corpus', models.TextField(null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='ListFilter',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('parameter', models.CharField(max_length=50, null=True, blank=True)),
                ('test', models.CharField(max_length=5, null=True, blank=True)),
                ('null', models.CharField(max_length=5, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='ListItem',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('body', models.CharField(max_length=255, null=True, blank=True)),
                ('extension', models.TextField(null=True, blank=True)),
                ('negative_value', models.CharField(max_length=128, null=True, blank=True)),
                ('index', models.IntegerField(null=True, blank=True)),
                ('instances', models.FloatField(null=True, blank=True)),
                ('source', models.CharField(max_length=10, null=True, blank=True)),
                ('list', models.ForeignKey(related_name='items', to='crux.List')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='ListItemAttributes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('list_item', models.ForeignKey(related_name='attributes', to='crux.ListItem')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='ListItemCodes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(max_length=50, null=True, blank=True)),
                ('value', models.CharField(max_length=50, null=True, blank=True)),
                ('list_item', models.ForeignKey(related_name='codes', to='crux.ListItem')),
                ('type', models.ForeignKey(related_name='+', blank=True, to='crux.DataTypes', null=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='ListItemQuantitativeSettings',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('value', models.CharField(max_length=50, null=True, blank=True)),
                ('promotion', models.CharField(max_length=1, null=True, blank=True)),
                ('before_launch_action', models.CharField(max_length=50, null=True, blank=True)),
                ('after_launch_action', models.CharField(max_length=50, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='ListItemTriggers',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(default=b'None', max_length=50, db_index=True)),
                ('include_modifiers', models.CharField(max_length=1, null=True, blank=True)),
                ('item', models.ForeignKey(related_name='triggers', to='crux.ListItem')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='ListOrString',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('value', models.CharField(max_length=50, null=True, blank=True)),
                ('list', models.ForeignKey(blank=True, to='crux.List', null=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Measurements',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('duration', models.ForeignKey(related_name='+', blank=True, to='crux.ListOrString', null=True)),
                ('frequency', models.ForeignKey(related_name='+', blank=True, to='crux.ListOrString', null=True)),
                ('item', models.ForeignKey(related_name='+', to='crux.ListItem')),
                ('method', models.ForeignKey(related_name='+', blank=True, to='crux.ListOrString', null=True)),
                ('site', models.ForeignKey(related_name='+', blank=True, to='crux.ListOrString', null=True)),
                ('strength', models.ForeignKey(related_name='+', blank=True, to='crux.ListOrString', null=True)),
                ('units', models.ForeignKey(related_name='+', blank=True, to='crux.ListOrString', null=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Outline',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(max_length=50, null=True, blank=True)),
                ('revision_level', models.CharField(max_length=5, null=True, blank=True)),
                ('description', models.CharField(max_length=50, null=True, blank=True)),
                ('record_type', models.CharField(max_length=50, null=True, blank=True)),
                ('available', models.CharField(max_length=1, null=True, blank=True)),
                ('expanded', models.CharField(max_length=1, null=True, blank=True)),
                ('index', models.IntegerField(null=True, blank=True)),
                ('instances', models.IntegerField(null=True, blank=True)),
                ('definition', models.CharField(max_length=255, null=True, blank=True)),
                ('ucase_abn', models.CharField(max_length=1, null=True, blank=True)),
                ('required', models.CharField(max_length=1, null=True, blank=True)),
                ('restrictions', models.CharField(max_length=128, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='OutlineAttributes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('outline', models.ForeignKey(related_name='attributes', to='crux.Outline')),
                ('value', models.ForeignKey(related_name='+', blank=True, to='crux.ListOrString', null=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='OutlineItem',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(max_length=100, null=True, blank=True)),
                ('label', models.CharField(max_length=50, null=True, blank=True)),
                ('value', models.CharField(max_length=100, null=True, blank=True)),
                ('index', models.IntegerField(null=True, blank=True)),
                ('selection_mode', models.CharField(blank=True, max_length=1, null=True, choices=[(b'Y', b'Yielded'), (b'N', b'Normal'), (b'A', b'All')])),
                ('sequence_index', models.IntegerField(null=True, blank=True)),
                ('show_code', models.CharField(max_length=1, null=True, blank=True)),
                ('required', models.CharField(max_length=1, null=True, blank=True)),
                ('hidden', models.CharField(max_length=1, null=True, blank=True)),
                ('adjectives', models.ManyToManyField(related_name='+', null=True, to='crux.Adjectives', blank=True)),
                ('list', models.ForeignKey(related_name='+', to='crux.List')),
                ('outline', models.ForeignKey(related_name='items', to='crux.Outline')),
                ('parent', models.ForeignKey(related_name='children', to='crux.OutlineItem')),
                ('section_modifier', models.ForeignKey(related_name='+', to='crux.List')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='OutlineItemAttributes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('outline_item', models.ForeignKey(related_name='attributes', to='crux.OutlineItem')),
                ('value', models.ForeignKey(related_name='+', blank=True, to='crux.ListOrString', null=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='OutlineUnderlayment',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(max_length=50)),
                ('outline', models.ForeignKey(related_name='underlayments', to='crux.Outline')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Underlayment',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(null=True, blank=True)),
                ('bits', models.TextField()),
                ('available', models.CharField(max_length=1)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.AddField(
            model_name='outlineunderlayment',
            name='underlayment',
            field=models.ForeignKey(related_name='outlines', to='crux.Underlayment'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='listitemquantitativesettings',
            name='measurements',
            field=models.ForeignKey(blank=True, to='crux.Measurements', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='listitemquantitativesettings',
            name='modifier',
            field=models.ForeignKey(related_name='dependant_items', blank=True, to='crux.List', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='listitemquantitativesettings',
            name='type',
            field=models.ForeignKey(related_name='+', blank=True, to='crux.DataTypes', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='listitemattributes',
            name='value',
            field=models.ForeignKey(related_name='+', blank=True, to='crux.ListOrString', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='listitem',
            name='measurements',
            field=models.ForeignKey(related_name='+', blank=True, to='crux.Measurements', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='listitem',
            name='qualitative_modifiers',
            field=models.ManyToManyField(to='crux.List', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='listitem',
            name='quantitative_settings',
            field=models.ForeignKey(related_name='+', blank=True, to='crux.ListItemQuantitativeSettings', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='listitem',
            name='type',
            field=models.ForeignKey(related_name='+', blank=True, to='crux.DataTypes', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='list',
            name='filter',
            field=models.ForeignKey(related_name='+', blank=True, to='crux.ListFilter', null=True),
            preserve_default=True,
        ),
    ]
