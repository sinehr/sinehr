# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('system', '0001_initial'),
        ('crux', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='basemodel',
            name='creation_user',
            field=models.ForeignKey(related_name='+', blank=True, to='system.ExtendedUserInformation', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basemodel',
            name='revision_user',
            field=models.ForeignKey(related_name='+', blank=True, to='system.ExtendedUserInformation', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='adjectives',
            name='outline',
            field=models.ForeignKey(related_name='adjectives', to='crux.Outline'),
            preserve_default=True,
        ),
    ]
