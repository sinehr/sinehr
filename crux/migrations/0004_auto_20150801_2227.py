# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import crux.models


class Migration(migrations.Migration):

    dependencies = [
        ('crux', '0003_auto_20150801_2226'),
    ]

    operations = [
        migrations.AlterField(
            model_name='basemodel',
            name='export_id',
            field=models.CharField(default=crux.models.default_export_uuid, unique=True, max_length=36, db_index=True),
            preserve_default=True,
        ),
    ]
