# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crux', '0002_auto_20150703_1801'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datatypes',
            name='name',
            field=models.CharField(unique=True, max_length=20, db_index=True),
            preserve_default=True,
        ),
    ]
