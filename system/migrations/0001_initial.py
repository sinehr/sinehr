# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('crux', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='EntityParameterDefinition',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(max_length=50)),
                ('default_value', models.CharField(max_length=50, null=True, blank=True)),
                ('returns', models.CharField(max_length=1, null=True, blank=True)),
                ('query', models.ForeignKey(related_name='+', blank=True, to='crux.ListOrString', null=True)),
                ('type', models.ForeignKey(related_name='+', blank=True, to='crux.DataTypes', null=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='ExtendedUserInformation',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('last_name', models.CharField(max_length=50, null=True, blank=True)),
                ('first_name', models.CharField(max_length=50, null=True, blank=True)),
                ('middle_name', models.CharField(max_length=50, null=True, blank=True)),
                ('title', models.CharField(max_length=50, null=True, blank=True)),
                ('employee_number', models.CharField(max_length=50, null=True, blank=True)),
                ('in_service_date', models.DateTimeField(null=True, blank=True)),
                ('out_of_service_date', models.DateTimeField(null=True, blank=True)),
                ('role', models.CharField(max_length=50, null=True, blank=True)),
                ('classification', models.CharField(max_length=1, null=True, blank=True)),
                ('active', models.CharField(max_length=1, null=True, blank=True)),
                ('user', models.ForeignKey(default=b'', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='ExtendedUserInformationAttributes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('value', models.CharField(max_length=50, null=True, blank=True)),
                ('user', models.ForeignKey(related_name='attributes', to='system.ExtendedUserInformation')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=50, null=True, blank=True)),
                ('facility_name', models.CharField(max_length=50, null=True, blank=True)),
                ('address_primary_line', models.CharField(max_length=50, null=True, blank=True)),
                ('address_secondary_line', models.CharField(max_length=50, null=True, blank=True)),
                ('address_city', models.CharField(max_length=50, null=True, blank=True)),
                ('address_state', models.CharField(max_length=50, null=True, blank=True)),
                ('address_country', models.CharField(max_length=50, null=True, blank=True)),
                ('address_zip', models.CharField(max_length=10, null=True, blank=True)),
                ('facility_phone', models.CharField(max_length=50, null=True, blank=True)),
                ('facility_fax', models.CharField(max_length=50, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='ParameterAttributes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('value', models.CharField(max_length=50, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Parameters',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(max_length=50)),
                ('value', models.CharField(max_length=50, null=True, blank=True)),
                ('date_value', models.DateTimeField(null=True, blank=True)),
                ('type', models.ForeignKey(related_name='+', blank=True, to='crux.DataTypes', null=True)),
                ('units', models.ForeignKey(related_name='+', blank=True, to='crux.ListOrString', null=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='ParametersCodes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(max_length=50, null=True, blank=True)),
                ('value', models.CharField(max_length=50, null=True, blank=True)),
                ('source', models.CharField(max_length=50, null=True, blank=True)),
                ('parameter', models.ForeignKey(related_name='codes', to='system.Parameters')),
                ('type', models.ForeignKey(related_name='+', blank=True, to='crux.DataTypes', null=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Rights',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('value', models.CharField(max_length=50, null=True, blank=True)),
                ('user', models.ForeignKey(related_name='rights', to='system.ExtendedUserInformation')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('key', models.CharField(max_length=50)),
                ('section', models.CharField(max_length=50)),
                ('name', models.CharField(max_length=50)),
                ('setting', models.TextField(null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.AddField(
            model_name='parameterattributes',
            name='parameter',
            field=models.ForeignKey(related_name='attributes', to='system.Parameters'),
            preserve_default=True,
        ),
    ]
