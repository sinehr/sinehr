import os
import uuid
from django.contrib.auth import authenticate
from django.shortcuts import render
from rest_framework.decorators import authentication_classes, api_view, permission_classes
from rest_framework.response import Response
from rest_framework import viewsets, status
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from system import models, serializers
from sinehr import settings
from importer import tasks
from django.contrib.auth.models import User


class ApiViewSet(viewsets.ModelViewSet):
    """
    Api View Set
    """
    permission_classes = (IsAuthenticated, )
    authentication_classes = (SessionAuthentication, TokenAuthentication, )


class TokenView(ObtainAuthToken):
    def post(self, request):
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            token, created = Token.objects.get_or_create(user=user)

            return Response({'token': token.key})
        return Response("Login failed", status=status.HTTP_401_UNAUTHORIZED)


obtain_auth_token = TokenView.as_view()

@api_view(['GET'])
@authentication_classes((SessionAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def start_import(request):
    source = request.GET['source']

    source_path = settings.IMPORTER_LOCATION_PATH + os.sep + source

    if not os.path.exists(source_path):
        return Response(
            {
                'code': 'SOURCE_DOES_NOT_EXIST',
                'description': 'The source file specified in the GET parameters does not exist.',
                'help_text': 'The specified source could not be found'
            },
            status=400
        )

    tasks.start_import.delay(os.path.realpath(source_path))

    return Response(
        {
            'code': 'SUCCESS',
            'description': 'Import started',
            'source': source,
            'help_text': 'Import started'
        },
        status=200
    )

@api_view(['GET', 'POST'])
@authentication_classes((SessionAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def importer(request):
    if request.method == 'POST':
        if len(request.FILES) == 0:
            return Response(
                {
                    'code': 'NO_SOURCE',
                    'description': 'No source file was supplied with importer POST request.',
                    'help_text': 'You must specify a source file to initiate an importer upload.'
                },
                status=400
            )
        elif len(request.FILES) > 1:
            return Response(
                {
                    'code': 'TOO_MANY_SOURCES',
                    'description': 'More than one source file was supplied with importer POST request.',
                    'help_text': 'You must specify a single source file to initiate an importer upload.'
                },
                status=400
            )
        else:
            for key in request.FILES:
                f = request.FILES[key]

                f_name = str(f.name)
                f_name_ext_index = f_name.rindex(os.extsep)

                filename = f.name[0:f_name_ext_index]
                fileextension = f.name[len(filename)+1:]

                if f.content_type != 'application/zip' and fileextension != 'zip':
                    return Response(
                        {
                            'code': 'SOURCE_INVALID',
                            'description': 'The supplied source file was not in zip format',
                            'help_text': 'The supplied source file was not in zip format'
                        },
                        status=400
                    )
                else:
                    filename = str(uuid.uuid4()) + os.extsep + fileextension

                    filepath = (settings.IMPORTER_LOCATION_PATH) + os.sep + filename

                    with open(filepath, 'wb+') as destination:
                        for chunk in f.chunks():
                            destination.write(chunk)

                    return Response(
                        {
                            'code': 'SUCCESS',
                            'description': 'The supplied source file successfully stored',
                            'help_text': 'The supplied source file successfully stored',
                            'filename': filename
                        },
                        status=200
                    )
    else:
        # GET
        import_sources = os.listdir(settings.IMPORTER_LOCATION_PATH)

        return Response(
            {
                'code': 'SUCCESS',
                'description': 'Available sources',
                'help_text': 'Available sources',
                'sources': import_sources
            },
            status=200
        )

def index(request):
    return render(request, 'common/index.html')

class UserViewSet(ApiViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


class ExtendedUserInformationAttributesViewSet(ApiViewSet):
    """
    ExtendedUserInformationAttributes
    """
    queryset = models.ExtendedUserInformationAttributes.objects.all()
    serializer_class = serializers.ExtendedUserInformationAttributesSerializer


class RightsViewSet(ApiViewSet):
    """
    Rights
    """
    queryset = models.Rights.objects.all()
    serializer_class = serializers.RightsSerializer


class ExtendedUserInformationViewSet(ApiViewSet):
    """
    ExtendedUserInformation
    """
    queryset = models.ExtendedUserInformation.objects.all()
    serializer_class = serializers.ExtendedUserInformationSerializer


class ParametersCodesViewSet(ApiViewSet):
    """
    ParametersCodes
    """
    queryset = models.ParametersCodes.objects.all()
    serializer_class = serializers.ParametersCodesSerializer


class ParameterAttributesViewSet(ApiViewSet):
    """
    ParameterAttributes
    """
    queryset = models.ParameterAttributes.objects.all()
    serializer_class = serializers.ParameterAttributesSerializer


class ParametersViewSet(ApiViewSet):
    """
    Parameters
    """
    queryset = models.Parameters.objects.all()
    serializer_class = serializers.ParametersSerializer


class LocationViewSet(ApiViewSet):
    """
    Location
    """
    queryset = models.Location.objects.all()
    serializer_class = serializers.LocationSerializer


class SettingsViewSet(ApiViewSet):
    """
    Settings
    """
    queryset = models.Settings.objects.all()
    serializer_class = serializers.SettingsSerializer


class EntityParameterDefinitionViewSet(ApiViewSet):
    """
    EntityParameterDefinition
    """
    queryset = models.EntityParameterDefinition.objects.all()
    serializer_class = serializers.EntityParameterDefinitionSerializer


