from rest_framework import serializers
from system import models
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User

class ExtendedUserInformationAttributesSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer(many=True, read_only=True)

    class Meta:
        model = models.ExtendedUserInformationAttributes


class RightsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Rights


class ExtendedUserInformationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.ExtendedUserInformation


class ParametersCodesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.ParametersCodes


class ParameterAttributesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.ParameterAttributes


class ParametersSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Parameters


class LocationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Location


class SettingsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Settings


class EntityParameterDefinitionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.EntityParameterDefinition


