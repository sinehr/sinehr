from django.contrib.auth.models import User
from django.db import models
from crux.models import BaseModel


class ExtendedUserInformationAttributes(BaseModel):
    user = models.ForeignKey("ExtendedUserInformation", related_name="attributes")
    name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    value = models.CharField(max_length=50, null=True, blank=True)


class Rights(BaseModel):
    user = models.ForeignKey("ExtendedUserInformation", related_name="rights")
    name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    value = models.CharField(max_length=50, null=True, blank=True)


class ExtendedUserInformation(BaseModel):
#AllUsers
    """
    Extended User Information
    """
    #id >> export_id
    user = models.ForeignKey(User, default='')
    last_name = models.CharField(max_length=50, null=True, blank=True)
    first_name = models.CharField(max_length=50, null=True, blank=True)
    #mi
    middle_name = models.CharField(max_length=50, null=True, blank=True)
    title = models.CharField(max_length=50, null=True, blank=True)
    #lic_no handled by ExtendedUserInformationAttributes with name of 'license_number'
    #dea handled by ExtendedUserInformationAttributes with name of 'dea_number'
    #emp_no
    employee_number = models.CharField(max_length=50, null=True, blank=True)
    #insvc_date
    in_service_date = models.DateTimeField(null=True, blank=True)
    #osvc_date
    out_of_service_date = models.DateTimeField(null=True, blank=True)
    #magic handled by ExtendedUserInformationAttributes with name of 'indica'
    role = models.CharField(max_length=50, null=True, blank=True)
    #cw
    classification = models.CharField(max_length=1, null=True, blank=True)
    #chartp handled by Rights with name of 'can_document_encounters'
    #refillp handled by Rights with name of 'can_refill'
    #viewp handled by Rights with name of 'can_view_entities'
    #enrollp handled by Rights with name of 'can_create_entities'
    #transp handled by Rights with name of 'can_create_transcribe'
    #cosignp handled by Rights with name of 'can_create_cosign'
    #active
    active = models.CharField(max_length=1, null=True, blank=True)
    #adminp handled by Rights with name of 'admin'
    #service handled by ExtendedUserInformationAttributes with name of 'service'
    #team handled by ExtendedUserInformationAttributes with name of 'service'
    #nickname handled by ExtendedUserInformationAttributes with name of 'service'
    #create_date handled by creation record
    #date_changed handled by revision record
    #pin handled by ExtendedUserInformationAttributes with name of 'pin'
    #passcode handled by ExtendedUserInformationAttributes with name of 'pass_code'
    #pwd_set_date handled by ExtendedUserInformationAttributes with name of 'pass_code_set_timestamp'
    #pwd_expire_date handled by ExtendedUserInformationAttributes with name of 'pass_code_expiry_timestamp'
    #pwd_change_reqd handled by ExtendedUserInformationAttributes with name of 'pass_code_required'
    #pwd_never_expires handled by ExtendedUserInformationAttributes with name of 'pass_code_never_expires'
    #create_by handled by creation record
    #change_by handled by revision record
    #phone handled by ExtendedUserInformationAttributes with name of 'phone'
    #cellphone handled by ExtendedUserInformationAttributes with name of 'cellphone'
    #fax handled by ExtendedUserInformationAttributes with name of 'fax'
    #pager handled by ExtendedUserInformationAttributes with name of 'pager'
    #email handled by ExtendedUserInformationAttributes with name of 'email'


class ParametersCodes(BaseModel):
    parameter = models.ForeignKey("Parameters", related_name="codes")
    name = models.CharField(max_length=50, null=True, blank=True)
    value = models.CharField(max_length=50, null=True, blank=True)
    source = models.CharField(max_length=50, null=True, blank=True)
    type = models.ForeignKey("crux.DataTypes", null=True, blank=True, related_name='+')


class ParameterAttributes(BaseModel):
    parameter = models.ForeignKey("Parameters", related_name="attributes")
    name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    value = models.CharField(max_length=50, null=True, blank=True)


class Parameters(BaseModel):
#GlobalParam
    #p_name
    name = models.CharField(max_length=50)
    #p_value
    value = models.CharField(max_length=50, null=True, blank=True)
    #p_data_type
    type = models.ForeignKey("crux.DataTypes", null=True, blank=True, related_name='+')
    #p_units
    units = models.ForeignKey("crux.ListOrString", null=True, blank=True, related_name='+')
    #p_date_value
    date_value = models.DateTimeField(null=True, blank=True)
    #p_fact_code/p_fact_code_data_type handled by ParametersCodes record named 'fact'
    #p_quant_fact_code/p_quant_fact_code_data_type handled by ParametersCodes record named 'quantitative'
    #p_coded_value,p_code_src handled by ParametersCodes record named 'standard'
    #p_semantic_code handled by ParametersCodes record named 'semantic'
    #date_changed handled by Revision record
    #acl handled by ParameterAttributes record named 'access_control_list'
    #p_return handled by ParameterAttributes record named 'return_value'


class Location(BaseModel):
#ServiceLocations
    #loc_code
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=50, null=True, blank=True)
    facility_name = models.CharField(max_length=50, null=True, blank=True)
    #fac_addr1
    address_primary_line = models.CharField(max_length=50, null=True, blank=True)
    #fac_addr2
    address_secondary_line = models.CharField(max_length=50, null=True, blank=True)
    #fac_city
    address_city = models.CharField(max_length=50, null=True, blank=True)
    #fac_state
    address_state = models.CharField(max_length=50, null=True, blank=True)
    address_country = models.CharField(max_length=50, null=True, blank=True)
    #fac_zip
    address_zip = models.CharField(max_length=10, null=True, blank=True)
    #fac_phone
    facility_phone = models.CharField(max_length=50, null=True, blank=True)
    #fac_fax
    facility_fax = models.CharField(max_length=50, null=True, blank=True)


class Settings(BaseModel):
#WorkstationSettings
    #rootkey
    key = models.CharField(max_length=50)
    #sectionname
    section = models.CharField(max_length=50)
    #keyname
    name = models.CharField(max_length=50)
    setting = models.TextField(null=True, blank=True)


class EntityParameterDefinition(BaseModel):
#EntityParamDefs
    #p_name
    name = models.CharField(max_length=50)
    #p_data_type
    type = models.ForeignKey("crux.DataTypes", null=True, blank=True, related_name='+')
    #p_default_value
    default_value = models.CharField(max_length=50, null=True, blank=True)
    #p_query
    query = models.ForeignKey("crux.ListOrString", null=True, blank=True, related_name='+')
    #date_changed handled by Creation model
    #p_return
    returns = models.CharField(max_length=1, null=True, blank=True)