from rest_framework import serializers
from lexicons import models


class LexiconSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Lexicon


class LexIndexSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.LexIndex


class CorporaAttributesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CorporaAttributes


class CorporaSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Corpora


