from django.db import models
from crux.models import BaseModel, DataTypes


class Lexicon(BaseModel):
    #code_val
    code = models.CharField(max_length=50)
    #src
    sources = models.CharField(max_length=50)
    full_text = models.TextField(null=True, blank=True)
    #sht_text
    short_text = models.CharField(max_length=255, null=True, blank=True)
    #changeindicator
    change_indicator = models.CharField(max_length=1, null=True, blank=True)
    #codestatus
    status = models.CharField(max_length=1, null=True, blank=True)


class LexIndex(BaseModel):
    #code_val
    code = models.CharField(max_length=50)
    word = models.CharField(max_length=50)
    #src
    source = models.CharField(max_length=50)


class CorporaAttributes(BaseModel):
    corpus = models.ForeignKey("Corpora", related_name="attributes")
    name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    type = models.ForeignKey(DataTypes, related_name="+")
    string = models.CharField(max_length=255, null=True, blank=True, db_index=True)
    text = models.TextField(null=True, blank=True)
    int = models.IntegerField(null=True, blank=True, db_index=True)
    float = models.FloatField(null=True, blank=True, db_index=True)
    date = models.DateTimeField(null=True, blank=True, db_index=True)


class Corpora(BaseModel):
    parent = models.ForeignKey('Corpora', related_name="children", null=True, blank=True, db_index=True)
    sequence_index = models.IntegerField(null=True, blank=True, db_index=True)