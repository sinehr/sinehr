from lexicons import models, serializers
from system.views import ApiViewSet


class LexiconViewSet(ApiViewSet):
    """
    Lexicon
    """
    queryset = models.Lexicon.objects.all()
    serializer_class = serializers.LexiconSerializer


class LexIndexViewSet(ApiViewSet):
    """
    LexIndex
    """
    queryset = models.LexIndex.objects.all()
    serializer_class = serializers.LexIndexSerializer


class CorporaAttributesViewSet(ApiViewSet):
    """
    CorporaAttributes
    """
    queryset = models.CorporaAttributes.objects.all()
    serializer_class = serializers.CorporaAttributesSerializer


class CorporaViewSet(ApiViewSet):
    """
    Corpora
    """
    queryset = models.Corpora.objects.all()
    serializer_class = serializers.CorporaSerializer


