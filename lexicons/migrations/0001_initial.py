# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crux', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Corpora',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('sequence_index', models.IntegerField(db_index=True, null=True, blank=True)),
                ('parent', models.ForeignKey(related_name='children', blank=True, to='lexicons.Corpora', null=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='CorporaAttributes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('name', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('string', models.CharField(db_index=True, max_length=255, null=True, blank=True)),
                ('text', models.TextField(null=True, blank=True)),
                ('int', models.IntegerField(db_index=True, null=True, blank=True)),
                ('float', models.FloatField(db_index=True, null=True, blank=True)),
                ('date', models.DateTimeField(db_index=True, null=True, blank=True)),
                ('corpus', models.ForeignKey(related_name='attributes', to='lexicons.Corpora')),
                ('type', models.ForeignKey(related_name='+', to='crux.DataTypes')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Lexicon',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('code', models.CharField(max_length=50)),
                ('sources', models.CharField(max_length=50)),
                ('full_text', models.TextField(null=True, blank=True)),
                ('short_text', models.CharField(max_length=255, null=True, blank=True)),
                ('change_indicator', models.CharField(max_length=1, null=True, blank=True)),
                ('status', models.CharField(max_length=1, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='LexIndex',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('code', models.CharField(max_length=50)),
                ('word', models.CharField(max_length=50)),
                ('source', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
    ]
