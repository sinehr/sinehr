from rest_framework import serializers
from entities import models


class AddressSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Address
        fields = ('type', 'primary_line', 'secondary_line', 'city', 'province', 'country', 'postal_code')


class LanguageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Language
        fields = ('type', 'value')


class PhoneSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Phone
        fields = ('type', 'value')


class RelationshipSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Relationship
        fields = ('related_entity', 'type')


class EntityAttributesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.EntityAttributes
        fields = ('name', 'value', 'searchable')

class EntityParameterSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.EntityParameter
        depth = 2
        fields = ('value', 'units', 'date_value', 'fact_code',
                  'quantitative_fact_code', 'coded_value', 'creation_date',
                  'creation_user', 'revision_date', 'revision_user',
                  'code_source', 'semantic_code', 'acl', 'definition',
                  'fact_code_data_type', 'quantitative_fact_code_data_type')


class EntitySectionsUsersSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.EntitySectionsUsers


class EntitySectionsAttributesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.EntitySectionsAttributes


class EntitySectionsSerializer(serializers.HyperlinkedModelSerializer):
    attributes = EntitySectionsAttributesSerializer(many=True, read_only=True)
    users = EntitySectionsUsersSerializer(many=True, read_only=True)

    class Meta:
        model = models.EntitySections


class EntitySerializer(serializers.HyperlinkedModelSerializer):
    attributes = EntityAttributesSerializer(many=True, read_only=True)
    phones = PhoneSerializer(many=True, read_only=True)
    languages = LanguageSerializer(many=True, read_only=True)
    addresses = AddressSerializer(many=True, read_only=True)
    relations = RelationshipSerializer(many=True, read_only=True)
    parameters = EntityParameterSerializer(many=True, read_only=True)
    sections = EntitySectionsSerializer(many=True, read_only=True)

    class Meta:
        model = models.Entity



