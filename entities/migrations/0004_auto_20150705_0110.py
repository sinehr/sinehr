# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0003_auto_20150704_1720'),
    ]

    operations = [
        migrations.RenameField(
            model_name='address',
            old_name='zip',
            new_name='postal_code',
        ),
        migrations.AlterField(
            model_name='language',
            name='entity',
            field=models.ForeignKey(related_name='languages', to='entities.Entity'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='phone',
            name='entity',
            field=models.ForeignKey(related_name='phones', to='entities.Entity'),
            preserve_default=True,
        ),
    ]
