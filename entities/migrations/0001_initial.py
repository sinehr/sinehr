# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('encounters', '0001_initial'),
        ('system', '0001_initial'),
        ('crux', '0002_auto_20150703_1801'),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(default=b'Primary', max_length=50)),
                ('primary_line', models.CharField(max_length=50, null=True, blank=True)),
                ('secondary_line', models.CharField(max_length=50, null=True, blank=True)),
                ('city', models.CharField(max_length=50, null=True, blank=True)),
                ('state', models.CharField(max_length=50, null=True, blank=True)),
                ('country', models.CharField(max_length=50, null=True, blank=True)),
                ('zip', models.CharField(max_length=10, null=True, blank=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Entity',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('record_number', models.CharField(db_index=True, max_length=50, null=True, blank=True)),
                ('start_date', models.DateTimeField(db_index=True, null=True, blank=True)),
                ('end_date', models.DateTimeField(db_index=True, null=True, blank=True)),
                ('encounterable', models.CharField(max_length=1, null=True, blank=True)),
                ('remark', models.TextField(null=True, blank=True)),
                ('last_encounter', models.ForeignKey(blank=True, to='encounters.Encounter', null=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EntityAttributes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(max_length=50, db_index=True)),
                ('value', models.CharField(max_length=255)),
                ('searchable', models.CharField(default=b'Y', max_length=1)),
                ('entity', models.ForeignKey(related_name='attributes', to='entities.Entity')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EntityParameter',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('value', models.CharField(max_length=255, null=True, blank=True)),
                ('units', models.CharField(max_length=50, null=True, blank=True)),
                ('date_value', models.DateTimeField(null=True, blank=True)),
                ('fact_code', models.CharField(max_length=50, null=True, blank=True)),
                ('quantitative_fact_code', models.CharField(max_length=50, null=True, blank=True)),
                ('coded_value', models.CharField(max_length=50, null=True, blank=True)),
                ('code_source', models.CharField(max_length=50, null=True, blank=True)),
                ('semantic_code', models.CharField(max_length=50, null=True, blank=True)),
                ('acl', models.CharField(max_length=255, null=True, blank=True)),
                ('definition', models.ForeignKey(related_name='parameters', to='system.EntityParameterDefinition')),
                ('entity', models.ForeignKey(related_name='parameters', to='entities.Entity')),
                ('fact_code_data_type', models.ForeignKey(related_name='+', blank=True, to='crux.DataTypes', null=True)),
                ('quantitative_fact_code_data_type', models.ForeignKey(related_name='+', blank=True, to='crux.DataTypes', null=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EntitySections',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('classification', models.CharField(max_length=50)),
                ('group', models.CharField(max_length=255, null=True, blank=True)),
                ('category', models.CharField(max_length=255, null=True, blank=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
                ('summary', models.CharField(max_length=255, null=True, blank=True)),
                ('remark', models.TextField(null=True, blank=True)),
                ('start_date', models.DateTimeField(null=True, blank=True)),
                ('restart_date', models.DateTimeField(null=True, blank=True)),
                ('end_date', models.DateTimeField(null=True, blank=True)),
                ('system_date', models.DateTimeField(null=True, blank=True)),
                ('priority', models.CharField(max_length=50, null=True, blank=True)),
                ('status', models.CharField(max_length=50, null=True, blank=True)),
                ('entity', models.ForeignKey(related_name='sections', to='entities.Entity')),
                ('episode', models.ForeignKey(related_name='+', blank=True, to='encounters.Episode', null=True)),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EntitySectionsAttributes',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(max_length=50, db_index=True)),
                ('value', models.CharField(max_length=255)),
                ('section_item', models.ForeignKey(related_name='attributes', to='entities.EntitySections')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='EntitySectionsUsers',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(max_length=50, db_index=True)),
                ('section_item', models.ForeignKey(related_name='users', to='entities.EntitySections')),
                ('user', models.ForeignKey(related_name='+', to='system.ExtendedUserInformation')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(default=b'Primary', max_length=50)),
                ('language', models.CharField(max_length=50)),
                ('entity', models.ForeignKey(to='entities.Entity')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Phone',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(default=b'Primary', max_length=50)),
                ('phone', models.CharField(max_length=50, null=True, blank=True)),
                ('entity', models.ForeignKey(to='entities.Entity')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.CreateModel(
            name='Relationship',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('type', models.CharField(default=b'Related', max_length=50, db_index=True)),
                ('entity', models.ForeignKey(related_name='entities', to='entities.Entity')),
                ('relation', models.ForeignKey(related_name='relations', to='entities.Entity')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
        migrations.AddField(
            model_name='address',
            name='entity',
            field=models.ForeignKey(to='entities.Entity'),
            preserve_default=True,
        ),
    ]
