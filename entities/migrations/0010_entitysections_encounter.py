# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('encounters', '0002_auto_20150703_1801'),
        ('entities', '0009_auto_20150716_0330'),
    ]

    operations = [
        migrations.AddField(
            model_name='entitysections',
            name='encounter',
            field=models.ForeignKey(related_name='+', blank=True, to='encounters.Encounter', null=True),
            preserve_default=True,
        ),
    ]
