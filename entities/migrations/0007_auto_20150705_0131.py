# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0006_auto_20150705_0122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='relationship',
            name='entity',
            field=models.ForeignKey(related_name='relations', to='entities.Entity'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relationship',
            name='related_entity',
            field=models.ForeignKey(related_name='+', to='entities.Entity'),
            preserve_default=True,
        ),
    ]
