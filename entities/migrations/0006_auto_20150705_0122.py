# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0005_auto_20150705_0112'),
    ]

    operations = [
        migrations.RenameField(
            model_name='relationship',
            old_name='relation',
            new_name='related_entity',
        ),
    ]
