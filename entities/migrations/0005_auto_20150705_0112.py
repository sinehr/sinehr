# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0004_auto_20150705_0110'),
    ]

    operations = [
        migrations.RenameField(
            model_name='address',
            old_name='state',
            new_name='province',
        ),
        migrations.AlterField(
            model_name='address',
            name='entity',
            field=models.ForeignKey(related_name='addresses', to='entities.Entity'),
            preserve_default=True,
        ),
    ]
