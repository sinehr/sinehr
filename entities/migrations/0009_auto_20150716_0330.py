# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0008_auto_20150716_0329'),
    ]

    operations = [
        migrations.RenameField(
            model_name='entitysectionsattributes',
            old_name='type',
            new_name='name',
        ),
    ]
