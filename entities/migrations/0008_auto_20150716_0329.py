# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0007_auto_20150705_0131'),
    ]

    operations = [
        migrations.RenameField(
            model_name='entitysectionsusers',
            old_name='type',
            new_name='name',
        ),
    ]
