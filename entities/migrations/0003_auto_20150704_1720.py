# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0002_auto_20150703_1824'),
    ]

    operations = [
        migrations.RenameField(
            model_name='language',
            old_name='language',
            new_name='value',
        ),
        migrations.RenameField(
            model_name='phone',
            old_name='phone',
            new_name='value',
        ),
    ]
