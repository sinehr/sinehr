from entities import serializers, models
from system.views import ApiViewSet


class AddressViewSet(ApiViewSet):
    """
    Address
    """
    queryset = models.Address.objects.all()
    serializer_class = serializers.AddressSerializer


class LanguageViewSet(ApiViewSet):
    """
    Language
    """
    queryset = models.Language.objects.all()
    serializer_class = serializers.LanguageSerializer


class PhoneViewSet(ApiViewSet):
    """
    Phone
    """
    queryset = models.Phone.objects.all()
    serializer_class = serializers.PhoneSerializer


class RelationshipViewSet(ApiViewSet):
    """
    Relationship
    """
    queryset = models.Relationship.objects.all()
    serializer_class = serializers.RelationshipSerializer


class EntityAttributesViewSet(ApiViewSet):
    """
    EntityAttributes
    """
    queryset = models.EntityAttributes.objects.all()
    serializer_class = serializers.EntityAttributesSerializer


class EntityViewSet(ApiViewSet):
    """
    Entity
    """
    queryset = models.Entity.objects.all()
    serializer_class = serializers.EntitySerializer


class EntityParameterViewSet(ApiViewSet):
    """
    EntityParameter
    """
    queryset = models.EntityParameter.objects.all()
    serializer_class = serializers.EntityParameterSerializer


class EntitySectionsUsersViewSet(ApiViewSet):
    """
    EntitySectionsUsers
    """
    queryset = models.EntitySectionsUsers.objects.all()
    serializer_class = serializers.EntitySectionsUsersSerializer


class EntitySectionsAttributesViewSet(ApiViewSet):
    """
    EntitySectionsAttributes
    """
    queryset = models.EntitySectionsAttributes.objects.all()
    serializer_class = serializers.EntitySectionsAttributesSerializer


class EntitySectionsViewSet(ApiViewSet):
    """
    EntitySections
    """
    queryset = models.EntitySections.objects.all()
    serializer_class = serializers.EntitySectionsSerializer


