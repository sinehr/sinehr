from django.db import models
from crux.models import BaseModel
from system.models import ExtendedUserInformation, EntityParameterDefinition


class Address(BaseModel):
    """
    Addresses for ${Entity} records
    """
    entity = models.ForeignKey("Entity", related_name='addresses')
    type = models.CharField(max_length=50, default="Primary")
    primary_line = models.CharField(max_length=50, null=True, blank=True)
    secondary_line = models.CharField(max_length=50, null=True, blank=True)
    city = models.CharField(max_length=50, null=True, blank=True)
    province = models.CharField(max_length=50, null=True, blank=True)
    country = models.CharField(max_length=50, null=True, blank=True)
    postal_code = models.CharField(max_length=10, null=True, blank=True)


class Language(BaseModel):
    """
    Languages for ${Entity} records
    """
    entity = models.ForeignKey("Entity", related_name='languages')
    type = models.CharField(max_length=50, default="Primary")
    value = models.CharField(max_length=50)


class Phone(BaseModel):
    """
    Phones for ${Entity} records
    """
    entity = models.ForeignKey("Entity", related_name='phones')
    type = models.CharField(max_length=50, default="Primary")
    value = models.CharField(max_length=50, null=True, blank=True)


class Relationship(BaseModel):
    """
    Relationships between ${Entity} records
    """
    entity = models.ForeignKey("Entity", related_name="relations")
    related_entity = models.ForeignKey("Entity", related_name="+")
    type = models.CharField(max_length=50, default="Related", db_index=True)


class EntityAttributes(BaseModel):
    """
    Attributes for an ${Entity} record
    """
    entity = models.ForeignKey("Entity", related_name="attributes", db_index=True)
    name = models.CharField(max_length=50, db_index=True)
    value = models.CharField(max_length=255)
    searchable = models.CharField(max_length=1, default="Y")


class Entity(BaseModel):
    """
    People, Organizations, or other legal entities
    """
    #pid >> export_id
    #mrn
    record_number = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    #fname >> attributes record with type of "first" unless is_org = Y then names record with type of "name"
    #mname >> attributes record with type of "middle"
    #lname >> attributes record with type of "last"
    #prefix >> attributes record with type of "prefix"
    #suffix >> attributes record with type of "suffix"
    #degree >> attributes record with type of "degree"
    #name_type_code >> attributes record with type of "name_type_code"
    #birthdate
    start_date = models.DateTimeField(null=True, blank=True, db_index=True)
    #deathdate
    end_date = models.DateTimeField(null=True, blank=True, db_index=True)

    #sex >> attributes record with type of "gender"

    # addr1 -> address.primary_line
    # addr2 -> address.secondary_line
    # city -> address.city
    # state -> address.state
    # country -> address.country
    # zip -> address.postal_code

    # homephone -> homephone.phone
    # workphone -> workphone.phone

    #ssn >> attributes record with type of "ssn" or "legal_id_number" if is_org = Y

    #ru_id + relat_to_ru -> relationship
    #mother_id + "Mother" -> relationship
    #father_id + "Father" -> relationship
    #guar_id + "Guarantor" -> relationship

    #is_pt
    encounterable = models.CharField(max_length=1, null=True, blank=True)

    #is_ru >> attributes record with type of "is_residential_unit_head"

    #is_org >> attributes record with type of "is_organization"

    #lwr >> attributes record with type of "lives_with_relationship"

    remark = models.TextField(null=True, blank=True)

    # handled by the creation model
    #created = models.DateTimeField(null=True, blank=True)
    #created_by = models.ForeignKey(ExtendedUserInformation, null=True, blank=True, related_name="entities_created")

    # handled by the revision model
    #date_changed -> updated
    #updated = models.DateTimeField(null=True, blank=True)
    #updated_by = models.ForeignKey(ExtendedUserInformation, null=True, blank=True, related_name="entities_updated")

    #last_visit
    last_encounter = models.ForeignKey('encounters.Encounter', null=True, blank=True)

    #race >> attributes record with type of "race"

    #preferred_lang -> language record


class EntityParameter(BaseModel):
    #pid
    entity = models.ForeignKey(Entity, related_name="parameters", db_index=True)
    #p_name handled by definition
    #p_data_type handled by definition
    definition = models.ForeignKey(EntityParameterDefinition, related_name="parameters", db_index=True)
    #p_value
    value = models.CharField(max_length=255, null=True, blank=True)
    #p_units
    units = models.CharField(max_length=50, null=True, blank=True)
    #date_value
    date_value = models.DateTimeField(null=True, blank=True)
    #p_fact_code
    fact_code = models.CharField(max_length=50, null=True, blank=True)
    #p_fact_code_data_type
    fact_code_data_type = models.ForeignKey("crux.DataTypes", null=True, blank=True, related_name='+')
    #p_quant_fact_code
    quantitative_fact_code = models.CharField(max_length=50, null=True, blank=True)
    #p_quant_fact_code_data_type
    quantitative_fact_code_data_type = models.ForeignKey("crux.DataTypes", null=True, blank=True, related_name='+')
    #p_coded_value
    coded_value = models.CharField(max_length=50, null=True, blank=True)
    #p_code_src
    code_source = models.CharField(max_length=50, null=True, blank=True)
    #p_semantic_code
    semantic_code = models.CharField(max_length=50, null=True, blank=True)
    #date_changed handled by Creation
    acl = models.CharField(max_length=255, null=True, blank=True)
    #p_return handled by definition


class EntitySectionsUsers(BaseModel):
    """
    User associations for an ${EntitySections} record
    """
    section_item = models.ForeignKey("EntitySections", related_name="users", db_index=True)
    name = models.CharField(max_length=50, db_index=True)
    user = models.ForeignKey(ExtendedUserInformation, related_name="+", db_index=True)


class EntitySectionsAttributes(BaseModel):
    """
    Attributes for an ${EntitySections} record
    """
    section_item = models.ForeignKey("EntitySections", related_name="attributes", db_index=True)
    name = models.CharField(max_length=50, db_index=True)
    value = models.CharField(max_length=255)


class EntitySections(BaseModel):
#Problems, Allergies, Cur Meds
    # prob_id, allerg_id, med_id >> export_id
    # "problem", "allergy", "alert", "curent_medication"
    classification = models.CharField(max_length=50)
    entity = models.ForeignKey(Entity, related_name="sections")
    group = models.CharField(max_length=255, null=True, blank=True)
    category = models.CharField(max_length=255, null=True, blank=True)
    name = models.CharField(max_length=255, null=True, blank=True)
    summary = models.CharField(max_length=255, null=True, blank=True)
    remark = models.TextField(null=True, blank=True)
    #coded_value handled by EntitySectionsAttributes record named "coded_value"
    #coded_source handled by EntitySectionsAttributes record named "coded_source"
    #examiner_id handled by EntitySectionsUsers record type = "examiner"
    #attending_id handled by EntitySectionsUsers record type = "attending"
    #supervisor_id handled by EntitySectionsUsers record type = "supervisor"
    #active_date, start_date
    start_date = models.DateTimeField(null=True, blank=True)
    #startby_id handled by EntitySectionsUsers record type = "started_by"
    #refill_date
    restart_date = models.DateTimeField(null=True, blank=True)
    #refillby_id handled by EntitySectionsUsers record type = "restarted_by"
    end_date = models.DateTimeField(null=True, blank=True)
    system_date = models.DateTimeField(null=True, blank=True)
    priority = models.CharField(max_length=50, null=True, blank=True)
    #allerg_status, prob_status, status_code
    status = models.CharField(max_length=50, null=True, blank=True)
    episode = models.ForeignKey('encounters.Episode', null=True, blank=True, related_name="+")
    encounter = models.ForeignKey('encounters.Encounter', null=True, blank=True, related_name="+")
    #prob_num handled by EntitySectionsAttributes record named "problem_number"
    #date_changed, updated_date, updatedby handled by Revision fields
