#!/bin/sh

find ./ -iname 'models.py' -exec grep --with-filename \{\} -e '^class ' \; | sed 's/\.py:class /|/g' | sed 's/(.*$//g' | sed 's/\.\/\///g' | sed 's/\//\./g' | gsed "s/^\(.*\)|\(.*\)$/router.register(r'\L\2\E', \1\.\2ViewSet)/g"