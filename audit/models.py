from django.db import models
from crux.models import BaseModel
from system.models import ExtendedUserInformation


class AuditHistory(BaseModel):
    model = models.TextField()
    change_date = models.DateTimeField()
    change_by = models.ForeignKey(ExtendedUserInformation, related_name="+")
    change_type = models.CharField(max_length=250)