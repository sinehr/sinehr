from rest_framework import serializers
from audit import models


class AuditHistorySerializer(serializers.ModelSerializer):
	class Meta:
		model = models.AuditHistory
