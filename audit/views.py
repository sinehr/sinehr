from audit import serializers, models
from system.views import ApiViewSet


class AuditHistoryViewSet(ApiViewSet):
    """
    AuditHistory
    """
    queryset = models.AuditHistory.objects.all()
    serializer_class = serializers.AuditHistorySerializer