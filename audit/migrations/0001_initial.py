# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('system', '0001_initial'),
        ('crux', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AuditHistory',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='crux.BaseModel')),
                ('model', models.TextField()),
                ('change_date', models.DateTimeField()),
                ('change_type', models.CharField(max_length=250)),
                ('change_by', models.ForeignKey(related_name='+', to='system.ExtendedUserInformation')),
            ],
            options={
            },
            bases=('crux.basemodel',),
        ),
    ]
