from datetime import date, datetime
# import json
import hashlib
import os
import django
from django.db import DataError

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sinehr.settings")
django.setup()

from celery import Celery
# from sinehr import settings
import csv
import tempfile
import uuid
import zipfile
import shutil

from entities import models as entities_models
from encounters import models as encounters_models
from system import models as system_models
from crux import models as crux_models

from django.contrib.auth.models import User

# app = Celery('tasks', broker='amqp://guest@localhost//')

app = Celery('tasks', backend='redis://localhost', broker='amqp://')


def get_user(username):
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        user = User.objects.create_user(username=username, password='pass')
        user.save()
        user = User.objects.get(username=username)

    return user


def get_extended_user_info(username):
    user = get_user(username)

    extended_user, created = system_models.ExtendedUserInformation.objects.get_or_create(user=user,
                                                                                         defaults={
                                                                                             'user': user
                                                                                         })
    return extended_user


@app.task
def import_entity(row):

    if row['birthdate'] == 'NULL':
        start_date = None
    else:
        start_date = str(row['birthdate']).split('/')
        start_date = date(int(start_date[2]), int(start_date[0]), int(start_date[1]))

    if row['deathdate'] == 'NULL':
        end_date = None
    else:
        end_date = str(row['deathdate']).split('/')
        end_date = date(int(end_date[2]), int(end_date[0]), int(end_date[1]))

    if row['pid'] == 'NULL':
        export_id = str(uuid.uuid4())
    else:
        export_id = row['pid']

    if row['mrn'] == 'NULL':
        record_number = None
    else:
        record_number = row['mrn']

    if row['is_pt'] == 'NULL':
        encounterable = None
    else:
        encounterable = row['is_pt']

    if row['remark'] == 'NULL':
        remark = None
    else:
        remark = row['remark']

    entity = {
        'export_id': export_id,
        'record_number': record_number,
        'start_date': start_date,
        'end_date': end_date,
        'encounterable': encounterable,
        'remark': remark,
        'last_encounter': None
    }

    if row['last_visit'] != 'NULL':
        encounter = {
            'export_id': hashlib.sha224("last_visit" + row['event_id']).hexdigest()
        }

        entity['last_encounter'] = encounters_models.Encounter.objects.get_or_create(export_id=encounter['export_id'],
                                                                                     defaults=encounter)

    print entity

    try:
        obj = entities_models.Entity.objects.get(export_id=entity['export_id'])

        entity['revision_date'] = datetime.now()
        entity['revision_user'] = None

        entities_models.Entity.objects.filter(export_id=entity['export_id']).update(**entity)
    except entities_models.Entity.DoesNotExist:
        obj = entities_models.Entity(**entity)
        obj.save()

    entity = obj

    # Cannot use this method due to need to update revsiion date.
    # entities_models.Entity.objects.update_or_create(export_id=entity['export_id'], defaults=entity)

    name_fields = ['prefix', 'fname', 'mname', 'lname', 'suffix', 'degree']

    if row['is_org'] == 'Y':
        save_entity_attribute('is_organization', row['is_org'], entity=entity)

        name = ''

        for field in name_fields:
            if row[field] != 'NULL' and row[field] != '':
                name += row[field] + ' '

        name = name.strip(' ')

        save_entity_attribute('name', name, entity=entity, searchable='Y')

        if row['ssn'] != 'NULL':
            save_entity_attribute('legal_id_number', row['ssn'], entity=entity, searchable='Y')
    else:
        for field in name_fields:
            if row[field] != 'NULL' and row[field] != '':
                searchable = 'N'
                attribute_name = field

                if attribute_name in ['fname', 'lname', 'mname']:
                    attribute_name = attribute_name.replace('fname', 'first')
                    attribute_name = attribute_name.replace('mname', 'middle')
                    attribute_name = attribute_name.replace('lname', 'last')
                    searchable = 'Y'

                save_entity_attribute(attribute_name, row[field], entity=entity, searchable=searchable)

        if row['sex'] != 'NULL':
            save_entity_attribute('gender', row['sex'], entity=entity, searchable='Y')

        if row['ssn'] != 'NULL':
            save_entity_attribute('ssn', row['ssn'], entity=entity, searchable='Y')

        if row['is_ru'] != 'NULL':
            save_entity_attribute('is_residential_unit_head', row['is_ru'], entity=entity)

        if row['lwr'] != 'NULL':
            save_entity_attribute('lives_with_relationship', row['lwr'], entity=entity)

        if row['race'] != 'NULL':
            save_entity_attribute('race', row['race'], entity=entity)

    if row['homephone'] != 'NULL':
        phone = {
            'entity': entity,
            'type': 'Home',
            'value': row['homephone']
        }

        entities_models.Phone.objects.update_or_create(entity=entity, type=phone['type'], defaults=phone)

    if row['workphone'] != 'NULL':
        phone = {
            'entity': entity,
            'type': 'Work',
            'value': row['workphone']
        }

        entities_models.Phone.objects.update_or_create(entity=entity, type=phone['type'], defaults=phone)

    if row['preferred_lang'] != 'NULL':
        language = {
            'entity': entity,
            'type': 'Preferred',
            'value': str(row['preferred_lang']).capitalize()
        }

        entities_models.Language.objects.update_or_create(entity=entity, type=language['type'], defaults=language)

    fields = ['addr1', 'addr2', 'city', 'state', 'country', 'zip']

    has_address = False

    for field in fields:
        if row[field] != 'NULL':
            has_address = True
            break

    if has_address:
        address = {
            'entity': entity,
            'type': 'Primary',
            'primary_line': row['addr1'] if row['addr1'] != 'NULL' else None,
            'secondary_line': row['addr2'] if row['addr2'] != 'NULL' else None,
            'city': row['city'] if row['city'] != 'NULL' else None,
            'province': row['state'] if row['state'] != 'NULL' else None,
            'country': row['country'] if row['country'] != 'NULL' else None,
            'postal_code': row['zip'] if row['zip'] != 'NULL' else None
        }

        entities_models.Address.objects.update_or_create(entity=entity, type=address['type'], defaults=address)

    if row['ru_id'] != 'NULL':
        related_entity_defaults = {
            'export_id': row['ru_id']
        }

        related_entity, created = entities_models.Entity.objects.get_or_create(export_id=row['ru_id'],
                                                                               defaults=related_entity_defaults)

        related_entity.save()

        relationship_type = 'Head of Household' if row['relat_to_ru'] == 'NULL' else str(row['relat_to_ru'])\
            .capitalize()

        relationship = {
            'entity': entity,
            'type': relationship_type,
            'related_entity': related_entity
        }

        entities_models.Relationship.objects.update_or_create(entity=entity,
                                                              type=relationship['type'],
                                                              defaults=relationship)

    if row['mother_id'] != 'NULL':
        related_entity_defaults = {
            'export_id': row['mother_id']
        }

        related_entity, created = entities_models.Entity.objects.get_or_create(export_id=row['mother_id'],
                                                                               defaults=related_entity_defaults)

        related_entity.save()

        relationship = {
            'entity': entity,
            'type': 'Mother',
            'related_entity': related_entity
        }

        entities_models.Relationship.objects.update_or_create(entity=entity,
                                                              type=relationship['type'],
                                                              defaults=relationship)

    if row['father_id'] != 'NULL':
        related_entity_defaults = {
            'export_id': row['father_id']
        }

        related_entity, created = entities_models.Entity.objects.get_or_create(export_id=row['father_id'],
                                                                               defaults=related_entity_defaults)

        related_entity.save()

        relationship = {
            'entity': entity,
            'type': 'Father',
            'related_entity': related_entity
        }

        entities_models.Relationship.objects.update_or_create(entity=entity,
                                                              type=relationship['type'],
                                                              defaults=relationship)

    if row['guar_id'] != 'NULL':
        related_entity_defaults = {
            'export_id': row['guar_id']
        }

        related_entity, created = entities_models.Entity.objects.get_or_create(export_id=row['guar_id'],
                                                                               defaults=related_entity_defaults)

        relationship = {
            'entity': entity,
            'type': 'Guarantor',
            'related_entity': related_entity
        }

        entities_models.Relationship.objects.update_or_create(entity=entity,
                                                              type=relationship['type'],
                                                              defaults=relationship)


def save_entity_attribute(name, value, entity, searchable='N', export_id=None):
    if export_id is None:
        export_id = str(uuid.uuid4())

    attribute = {
        'entity': entity,
    }

    if value != 'NULL':
        attribute['export_id'] = export_id
        attribute['name'] = name
        attribute['searchable'] = searchable
        attribute['value'] = value

        entities_models.EntityAttributes.objects.update_or_create(name=attribute['name'],
                                                                  entity=entity,
                                                                  defaults=attribute)


def entity_table(source):

    # entities_models.Entity.objects.all().delete()

    f = open(source, "rb")
    names = csv.reader(f).next()
    names = [x.lower() for x in names]
    reader = csv.DictReader(f, fieldnames=names)

    for row in reader:
        import_entity.delay(row)


@app.task
def import_entity_param_defs(row):

    print row['p_query']

    parameter_definition = {
        'type': get_data_type(row['p_data_type']),
        'name': row['p_name'],
        'default_value': row['p_default_value'] if row['p_default_value'] != 'NULL' else None,
        'returns': row['p_return'] if row['p_return'] != 'NULL' else None,
        'revision_date': datetime.now(),
        'revision_user': None,
        'query': get_list_or_string(row['p_query'])
    }

    system_models.EntityParameterDefinition.objects.update_or_create(name=parameter_definition['name'],
                                                                     defaults=parameter_definition)


@app.task
def import_entity_param(row):
    entity, created = entities_models.Entity.objects.get_or_create(export_id=row['pid'],
                                                                   defaults={'export_id': row['pid']})

    definition, created = system_models.EntityParameterDefinition.objects.get_or_create(
        name=row['p_name'],
        type=get_data_type(row['p_data_type']),
        defaults={
            'name': row['p_name'],
            'type': get_data_type(row['p_data_type']),
            'returns': row['p_return'] if row['p_return'] != 'NULL' else None
        })

    if row['p_date_value'] == 'NULL':
        date_value = None
    else:
        date_value = datetime.strptime(row['p_date_value'], '%m/%d/%Y %I:%M:%S %p')

    if row['date_changed'] == 'NULL':
        creation_date = None
    else:
        creation_date = datetime.strptime(row['date_changed'], '%m/%d/%Y %I:%M:%S %p')

    parameter = {
        'entity': entity,
        'definition': definition,
        'value': row['p_value'],
        'units': row['p_units'],
        'date_value': date_value,
        'fact_code': row['p_fact_code'],
        'fact_code_data_type': get_data_type(row['p_fact_code_data_type']),
        'quantitative_fact_code': row['p_quant_fact_code'],
        'quantitative_fact_code_data_type': get_data_type(row['p_quant_fact_code_data_type']),
        'coded_value': row['p_coded_value'],
        'code_source': row['p_code_src'],
        'semantic_code': row['p_semantic_code'],
        'acl': row['acl'],
        'revision_date': datetime.now(),
        'revision_user': None,
        'creation_date': creation_date,
        'creation_user': None
    }

    entities_models.EntityParameter.objects.update_or_create(entity=entity,
                                                             definition=definition,
                                                             defaults=parameter)


def get_date_from_field(field):
    if field == 'NULL':
        date_value = None
    elif str(field).lower().endswith("m"):
        date_value = datetime.strptime(field, '%m/%d/%Y %I:%M:%S %p')
    else:
        date_value = datetime.strptime(field + ' 12:00:00 AM', '%m/%d/%Y %I:%M:%S %p')

    return date_value


def save_entity_section_attribute(name, value, section, export_id=None):
    if export_id is None:
        export_id = str(uuid.uuid4())

    attribute = {
        'section_item': section,
    }

    if value != 'NULL' and value is not None:
        attribute['export_id'] = export_id
        attribute['name'] = name
        attribute['value'] = value

        entities_models.EntitySectionsAttributes.objects.update_or_create(name=attribute['name'],
                                                                          section_item=section,
                                                                          defaults=attribute)


def save_entity_section_user(name, username, section, export_id=None):
    if export_id is None:
        export_id = str(uuid.uuid4())

    record = {
        'section_item': section,
    }

    if username != 'NULL':
        record['export_id'] = export_id
        record['name'] = name
        record['user'] = get_extended_user_info(username)

        entities_models.EntitySectionsUsers.objects.update_or_create(name=record['name'],
                                                                     section_item=section,
                                                                     defaults=record)


@app.task
def import_problem(row):
    entity, created = entities_models.Entity.objects.get_or_create(export_id=row['pid'],
                                                                   defaults={'export_id': row['pid']})

    episodeid = hashlib.sha224("episode" + row['episodeid']).hexdigest()

    episode, created = encounters_models.Episode.objects.get_or_create(export_id=episodeid,
                                                                       defaults={'export_id': episodeid})

    classification = "problem"
    start_date = get_date_from_field(row['active_date'])
    restart_date = None  # No restart date for problems
    end_date = get_date_from_field(row['inactive_date'])
    system_date = get_date_from_field(row['system_date'])
    revision_date = get_date_from_field(row['date_changed'])

    if revision_date is None:
        revision_date = datetime.now()

    if system_date is None:
        system_date = revision_date

    problem = {
        'export_id': hashlib.sha224("problem" + row['prob_id']).hexdigest(),
        'entity': entity,
        'classification': classification,
        'name': str(row['problem']).lower().replace(' ', '_'),
        'summary': row['problem'],
        'remark': row['remark'],
        'start_date': start_date,
        'restart_date': restart_date,
        'end_date': end_date,
        'system_date': system_date,
        'priority': row['priority'],
        'status': row['prob_status'],
        'episode': episode,
        'revision_date': revision_date,
        'revision_user': None,
        'creation_date': system_date,
        'creation_user': None
    }

    entities_models.EntitySections.objects.update_or_create(export_id=problem['export_id'],
                                                            defaults=problem)

    section_item = entities_models.EntitySections.objects.get(export_id=problem['export_id'])

    save_entity_section_attribute('coded_value', row['coded_value'], section_item)

    save_entity_section_attribute('coded_source', row['code_src'], section_item)

    save_entity_section_attribute('problem_number', row['prob_num'], section_item)

    try:
        save_entity_section_user('examiner', row['examiner_id'], section_item)
    except DataError:
        save_entity_section_attribute('examiner', row['examiner_id'], section_item)

    try:
        save_entity_section_user('attending', row['attending_id'], section_item)
    except DataError:
        save_entity_section_attribute('attending', row['attending_id'], section_item)


@app.task
def import_allergy(row):
    entity, created = entities_models.Entity.objects.get_or_create(export_id=row['pid'],
                                                                   defaults={'export_id': row['pid']})

    episodeid = hashlib.sha224("episode" + row['episodeid']).hexdigest()

    episode, created = encounters_models.Episode.objects.get_or_create(export_id=episodeid,
                                                                       defaults={'export_id': episodeid})

    classification = "allergy"
    start_date = get_date_from_field(row['active_date'])
    restart_date = None  # No restart date for allergies
    end_date = get_date_from_field(row['inactive_date'])
    system_date = get_date_from_field(row['system_date'])
    revision_date = get_date_from_field(row['date_changed'])

    if revision_date is None:
        revision_date = datetime.now()

    if system_date is None:
        system_date = revision_date

    allergy = {
        'export_id': hashlib.sha224("allergy" + row['allerg_id']).hexdigest(),
        'entity': entity,
        'classification': classification,
        'name': str(row['allergy']).lower().replace(' ', '_'),
        'summary': row['allergy'],
        'remark': row['remark'],
        'start_date': start_date,
        'restart_date': restart_date,
        'end_date': end_date,
        'system_date': system_date,
        'priority': row['priority'],
        'status': row['allerg_status'],
        'episode': episode,
        'revision_date': revision_date,
        'revision_user': None,
        'creation_date': system_date,
        'creation_user': None
    }

    entities_models.EntitySections.objects.update_or_create(export_id=allergy['export_id'],
                                                            defaults=allergy)

    section_item = entities_models.EntitySections.objects.get(export_id=allergy['export_id'])

    save_entity_section_attribute('coded_value', row['coded_value'], section_item)

    save_entity_section_attribute('coded_source', row['code_src'], section_item)

    try:
        save_entity_section_user('examiner', row['examiner_id'], section_item)
    except DataError:
        save_entity_section_attribute('examiner', row['examiner_id'], section_item)

    try:
        save_entity_section_user('attending', row['attending_id'], section_item)
    except DataError:
        save_entity_section_attribute('attending', row['attending_id'], section_item)


@app.task
def import_current_medication(row):
    entity, created = entities_models.Entity.objects.get_or_create(export_id=row['pid'],
                                                                   defaults={'export_id': row['pid']})

    episodeid = hashlib.sha224("episode" + row['episodeid']).hexdigest()

    episode, created = encounters_models.Episode.objects.get_or_create(export_id=episodeid,
                                                                       defaults={'export_id': episodeid})

    eventid = hashlib.sha224("event" + row['event_id']).hexdigest()

    encounter, created = encounters_models.Encounter.objects.get_or_create(export_id=eventid,
                                                                           defaults={'export_id': eventid})

    classification = "current_medication"
    start_date = get_date_from_field(row['start_date'])
    restart_date = get_date_from_field(row['refill_date'])
    end_date = None
    system_date = get_date_from_field(row['system_date'])
    revision_date = get_date_from_field(row['date_changed'])

    if revision_date is None:
        revision_date = datetime.now()

    if system_date is None:
        system_date = revision_date

    medication = {
        'export_id': hashlib.sha224("medication" + row['med_id']).hexdigest(),
        'entity': entity,
        'classification': classification,
        'name': row['item_name'],
        'summary': row['item_value'],
        'remark': row['item_comment'],
        'start_date': start_date,
        'restart_date': restart_date,
        'end_date': end_date,
        'system_date': system_date,
        'priority': None,
        'status': row['status_code'],
        'episode': episode,
        'encounter': encounter,
        'revision_date': revision_date,
        'revision_user': None,
        'creation_date': system_date,
        'creation_user': None
    }

    entities_models.EntitySections.objects.update_or_create(export_id=medication['export_id'],
                                                            defaults=medication)

    section_item = entities_models.EntitySections.objects.get(export_id=medication['export_id'])

    save_entity_section_attribute('coded_value', row['coded_value'], section_item)

    save_entity_section_attribute('coded_source', row['code_src'], section_item)

    save_entity_section_attribute('action_taken', row['action_taken'], section_item)

    save_entity_section_attribute('updated_date', get_date_from_field(row['upd_date']), section_item)

    try:
        save_entity_section_user('started_by', row['startby_id'], section_item)
    except DataError:
        save_entity_section_attribute('started_by', row['startby_id'], section_item)

    try:
        save_entity_section_user('restart_by', row['refillby_id'], section_item)
    except DataError:
        save_entity_section_attribute('restart_by', row['refillby_id'], section_item)

    try:
        save_entity_section_user('updated_by', row['updby_id'], section_item)
    except DataError:
        save_entity_section_attribute('updated_by', row['updby_id'], section_item)


def entity_param_table(source):

    # entities_models.EntityParameter.objects.all().delete()

    f = open(source, "rb")
    names = csv.reader(f).next()
    names = [x.lower() for x in names]
    reader = csv.DictReader(f, fieldnames=names)

    for row in reader:
        import_entity_param.delay(row)


def problems_table(source):

    # entities_models.EntitySections.objects.get(classification="problem").delete()

    f = open(source, "rb")
    names = csv.reader(f).next()
    names = [x.lower() for x in names]
    reader = csv.DictReader(f, fieldnames=names)

    for row in reader:
        import_problem.delay(row)


def allergies_table(source):

    # entities_models.EntitySections.objects.get(classification="allergy").delete()

    f = open(source, "rb")
    names = csv.reader(f).next()
    names = [x.lower() for x in names]
    reader = csv.DictReader(f, fieldnames=names)

    for row in reader:
        import_allergy.delay(row)


def current_medication_table(source):

    # entities_models.EntitySections.objects.get(classification="current_medication").delete()

    f = open(source, "rb")
    names = csv.reader(f).next()
    names = [x.lower() for x in names]
    reader = csv.DictReader(f, fieldnames=names)

    for row in reader:
        import_current_medication.delay(row)


def entity_param_defs_table(source):

    # entities_models.EntityParameterDefinition.objects.all().delete()

    f = open(source, "rb")
    names = csv.reader(f).next()
    names = [x.lower() for x in names]
    reader = csv.DictReader(f, fieldnames=names)

    for row in reader:
        import_entity_param_defs.delay(row)


def get_data_type(data_type):
    if data_type != 'NULL':
        data_type, created = crux_models.DataTypes.objects.get_or_create(name=data_type, defaults={'name': data_type})
        return data_type
    else:
        return None


def get_list(list_name, section='CODE', type_field='application'):
    list_instance, created = crux_models.List.objects.get_or_create(name=list_name,
                                                                    section=section,
                                                                    type=type_field,
                                                                    defaults={'name': list_name,
                                                                              'section': section,
                                                                              'type': type_field})

    return list_instance


def get_list_or_string(list_or_string):
    if list_or_string != 'NULL':

        list_or_string_dict = {
            'list': None,
            'value': None
        }

        if list_or_string[0] == '?':
            list_or_string_dict['list'] = get_list(list_or_string[1:])
        else:
            list_or_string_dict['value'] = list_or_string

        list_or_string, created = crux_models.ListOrString.objects.get_or_create(list=list_or_string_dict['list'],
                                                                                 value=list_or_string_dict['value'],
                                                                                 defaults=list_or_string_dict)

        return list_or_string
    else:
        return None


@app.task
def start_import(source):

    working_folder = tempfile.gettempdir() + os.sep + str(uuid.uuid4())

    os.mkdir(working_folder)

    print source
    print working_folder

    with zipfile.ZipFile(source, "r") as z:
        z.extractall(working_folder)

    export_path = working_folder + os.sep + "cwexport" + os.sep

    databases = list()

    databases.append(export_path + "charts" + os.sep)

    vocabulary_files_path = export_path + "vocab_files" + os.sep

    for vocabulary in os.listdir(vocabulary_files_path):
        databases.append(os.path.realpath(vocabulary_files_path + vocabulary) + os.sep)

    for database in databases:
        for table in os.listdir(database):

            l_table_name = table.lower()

            if l_table_name.endswith(".csv"):
                if l_table_name[:-4] == "entity":
                    entity_table(database + os.sep + table)
                if l_table_name[:-4] == "entity_param_defs":
                    entity_param_defs_table(database + os.sep + table)
                if l_table_name[:-4] == "entity_param":
                    entity_param_table(database + os.sep + table)
                if l_table_name[:-4] == "problems":
                    problems_table(database + os.sep + table)
                if l_table_name[:-4] == "allergies":
                    allergies_table(database + os.sep + table)
                if l_table_name[:-4] == "cur_meds":
                    current_medication_table(database + os.sep + table)

    shutil.rmtree(working_folder)
